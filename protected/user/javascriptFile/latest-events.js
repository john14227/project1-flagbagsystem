let events;
let user;

async function fetchData(){
    const res = await fetch('/events/unregistered/');
    events = await res.json();
    populateData(events);
}

function populateData(events){
    const events_result = document.querySelector('#event_result');
    events_result.innerHTML = "";
    for(let event of events){
        events_result.innerHTML += `
        <tr>
            <td>${event.event_name}</td>
            <td>${new Date(event.event_date).toDateString()}</td>
            <td>${event.event_start_time}</td>
            <td>${event.event_end_time}</td>
            <td>
                <button id="myBtn2" onclick="location.href ='register-events.html?${event.id}'">Register</button>
            </td>
        </tr>
        `
    }
}

window.onload = async ()=>{
    const res = await fetch('/users/currentUser', {
        method: "GET"
    });
    user = await res.json()
    await fetchData();
}

btn.onclick = function() {
    modal.style.display = "block";
  }