let event;
let user;
let id;
let eventRegisters;

async function fetchData(){
    id = location.search;
    id = id.substr(1)
    const res = await fetch('/events/'+id);

    event = await res.json();

    const event_name = document.querySelector('#event_name');
    event_name.innerHTML = event[0].event_name;

    const event_date = document.querySelector('#event_date');
    event_date.innerHTML = new Date(event[0].event_date).toDateString();

    const event_start_time = document.querySelector('#event_start_time');
    event_start_time.innerHTML = event[0].event_start_time;

    const event_end_time = document.querySelector('#event_end_time');
    event_end_time.innerHTML = event[0].event_end_time;
    
}


async function createRegisterEventFetchAsync(eventRegister){
    const res = await fetch('/event-register',{
        method: 'POST',
        headers:{
            "Content-Type":"application/json"
        },
        body: JSON.stringify(eventRegister)
    });
    const result = await res.json();
    // document.querySelector('#result').append(JSON.stringify(result))
}

document.querySelector('#create-register-form')
        .addEventListener('submit',function(eventRegister){
            eventRegister.preventDefault();
    
            const formData = {
                event_id: id
            };
            // createVolunteerFetchAsync(volunteerFromData);
            // const form = this
            // for(let input of form){
            //     if(!['submit'].includes(input.type)){
            //         formData[input.name] = input.value;
            //     }
            // }
            createRegisterEventFetchAsync(formData);
            location.href="my-events.html"
        });


window.onload= async ()=> {
    const res = await fetch('/users/currentUser', {
        method: "GET"
    });
    user = await res.json()
    await fetchData();
}

