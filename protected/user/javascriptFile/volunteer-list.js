//import fetch from 'cross-fetch'

let eventId = String(location.search.substr(1));;
let userEventdetails;
let num;
let volunteerInfo;
let volunteerData;



async function fetchActivity(){
    const res = await fetch('/volunteers/'+ eventId);
    userEventdetails = await res.json();
    console.log(userEventdetails)
}


function populateData(num, volunteerInfo){
    const volunteer_list = document.querySelector('#volunteer-list');
    volunteer_list.innerHTML = "";
    for(let i = 0; i < num ; i++){
        volunteer_list.innerHTML += `
        <tr>
            <td><input type = "text" name = "QR_code" class = "QR-code" style="width:70%" disabled></td>
            <td><input type = "text" name = "chi_fullname" class = "Chinese-name" style="width:70%"></td>
            <td><input type = "text" name = "eng_fullname" class = "English-name" style="width:70%"></td>
            <td><input type = "text" name = "contact_person" class = "contactPerson" style="width:70%"></td>
            <td><input type = "text" name = "contact_num" class = "telephone" style="width:70%"></td>
        </tr>
        `
        //document.querySelector('.inputs').style.Width = '50%';
    }
    const qrCodeValue = document.querySelectorAll('.QR-code');
    const chineseName = document.querySelectorAll('.Chinese-name');
    const englishName = document.querySelectorAll('.English-name');
    const contactPerson = document.querySelectorAll('.contactPerson');
    const telephone = document.querySelectorAll('.telephone');

    for (let i = 0; i < volunteerInfo.length; i++){
        //console.log(i, volunteerList[i]);
        qrCodeValue[i].value = volunteerInfo[i].QR_code;
        chineseName[i].value = volunteerInfo[i].chi_fullname;
        englishName[i].value = volunteerInfo[i].eng_fullname;
        contactPerson[i].value = volunteerInfo[i].contact_person;
        telephone[i].value = volunteerInfo[i].contact_num;
    }
    
    document.querySelector('#volunteer-list-save').innerHTML = 
    `<button type="submit" onclick="this.form.submited=this.value;" id = "save" form="volunteer-form" name = "save" value="save" style="width:70%" >儲存</button>`;

    document.querySelector('#volunteer-list-submit').innerHTML = 
    `<button type="submit" onclick="this.form.submited=this.value;" id = "submit" form="volunteer-form" name = "submit" value="submit" style="width:70%" >提交</button>`;
}

async function createVolunteerList(volunteerList){
    const volunteerForm = {};
    volunteerForm.eventId = Number(eventId);
    volunteerForm.organizationId = organizationId;
    volunteerForm.volunteerList = volunteerList;
    console.log(volunteerForm);
    console.log('save');
    const res = await fetch("/volunteers/volunteerList",{
        method: 'PUT',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(volunteerForm)
    })
    await res.json();
}

async function submitVolunteerList(volunteerList){
    const volunteerForm = {};
    volunteerForm.eventId = Number(eventId);
    volunteerForm.organizationId = organizationId;
    volunteerForm.volunteerList = volunteerList;
    console.log(volunteerForm);
    console.log('submit');
    const res = await fetch("/volunteers/submission",{
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(volunteerForm)
    })
    await res.json();
}


window.onload = async ()=>{
    await fetchActivity();
    //console.log(user);
    //console.log(userEventdetails);
    num = userEventdetails.volunteerNum;
    volunteerInfo = userEventdetails.volunteerList;
    organizationId = userEventdetails.organizationId;
    console.log(userEventdetails);
    populateData(num, volunteerInfo);
    volunteerData = formData();
    console.log(volunteerData);
    // document.querySelector('#save').addEventListener('click',async function(event){
    //     event.preventDefault();
    //     await createVolunteerList(volunteerData);
    // });
    
    
    // document.querySelector('#submit').addEventListener('click',async function(event){
    //     event.preventDefault();
    //     await submitVolunteerList(volunteerData);
    // });
}

function formData(){
    let formData = [];
    let volunteerData;
    const qrCodeValue = document.querySelectorAll('.QR-code');
    const chineseName = document.querySelectorAll('.Chinese-name');
    const englishName = document.querySelectorAll('.English-name');
    const contactPerson = document.querySelectorAll('.contactPerson');
    const telephone = document.querySelectorAll('.telephone');
    
    for (let i = 0; i < num; i++){
        volunteerData = {}
        //console.log(i, volunteerList[i]);
        volunteerData['QR_code'] = qrCodeValue[i].value;  
        volunteerData['chi_fullname'] = chineseName[i].value;        
        volunteerData['eng_fullname'] = englishName[i].value;         
        volunteerData['contact_person'] = contactPerson[i].value;
        volunteerData['contact_num'] = telephone[i].value    

        formData.push(volunteerData);
    }
    formData = formData.filter(function(volunteer){
        return volunteer.QR_code.length > 0;
        // /[\u4e00-\u9fa5]/g.test(volunteer.chi_fullname)
        //         && /[A-Za-z]/g.test(volunteer.eng_fullname)
        //         && /[A-Za-z\u4e00-\u9fa5]/g.test(volunteer.contact_person)
        //         && /[0-9]{8}/g.test(volunteer.contact_num)
        //         && 

                //volunteer.hasOwnProperty('QR_code');
    });
    
    console.log(formData);
    return formData;
}




document.querySelector('#volunteer-form').addEventListener('submit',async function(event){
    event.preventDefault();
    let formData = [];
    let volunteerData;
    const qrCodeValue = document.querySelectorAll('.QR-code');
    const chineseName = document.querySelectorAll('.Chinese-name');
    const englishName = document.querySelectorAll('.English-name');
    const contactPerson = document.querySelectorAll('.contactPerson');
    const telephone = document.querySelectorAll('.telephone');
    
    for (let i = 0; i < num; i++){
        volunteerData = {}
        //console.log(i, volunteerList[i]);
        volunteerData['QR_code'] = qrCodeValue[i].value;  
        volunteerData['chi_fullname'] = chineseName[i].value;        
        volunteerData['eng_fullname'] = englishName[i].value;         
        volunteerData['contact_person'] = contactPerson[i].value;
        volunteerData['contact_num'] = telephone[i].value    

        formData.push(volunteerData);
    }
    formData = formData.filter(function(volunteer){
        return volunteer.QR_code.length > 0;
        // /[\u4e00-\u9fa5]/g.test(volunteer.chi_fullname)
        //         && /[A-Za-z]/g.test(volunteer.eng_fullname)
        //         && /[A-Za-z\u4e00-\u9fa5]/g.test(volunteer.contact_person)
        //         && /[0-9]{8}/g.test(volunteer.contact_num)
        //         && 

                //volunteer.hasOwnProperty('QR_code');
    });
    
    console.log(formData);
    //console.log(this.submited);
    if (this.submited == 'save'){
        await createVolunteerList(formData);  
    } else {
        await submitVolunteerList(formData);
    }
    //await createVolunteerList(formData);

});

