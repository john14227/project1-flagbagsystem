let eventRegisters;
let user;
let userId;
let registerEventList;

async function fetchData(){
    userId = String(user.id);
    const res = await fetch('/event-register/user/'+userId);
    eventRegisters = await res.json();
    console.log(userId);
    console.log(eventRegisters);
    populateData(eventRegisters);
}

function populateData(eventRegisters){
    const eventRegister_result = document.querySelector('#event_info_result');
    eventRegister_result.innerHTML = "";
    for(let eventRegister of eventRegisters){
        const buttonName = "button-"+(eventRegister.event_name).replace(/\s/g,'');
        eventRegister_result.innerHTML += `
        <tr>
            <td>${eventRegister.event_name}</td>
            <td>${new Date(eventRegister.event_date).toDateString()}</td>
            <td>${eventRegister.event_start_time}</td>
            <td>${eventRegister.event_end_time}</td>
            <td><button id = ${buttonName}>新增義工</button></td>
            <td><button id = "myBtn3" >修改</button>
                <button id = "myBtn4">刪除</button></td>
        </tr>
        `
        document.getElementById(buttonName).setAttribute('class','event-register');
        //console.log(document.getElementById(buttonName));
    }
    
}
window.onload= async ()=>{
    const res = await fetch('/users/currentUser', {
        method: "GET"
    });
    user = await res.json();
    await fetchData();
    registerEventList = document.querySelectorAll('.event-register');
    for (let i = 0 ; i < registerEventList.length; i++){
        registerEventList[i].addEventListener('click',function(){
            //console.log('hi');
            //console.log(eventRegisters[i].event_name);
            //console.log(eventRegisters[i]);
            location.href = `volunteer-list.html?${eventRegisters[i].id}`;
        });
    }
}


// document.addEventListener('readystatechange',function(){
//     if (event.target.readyState == "complete"){
//         console.log(registerEventList)
//     };
// });



