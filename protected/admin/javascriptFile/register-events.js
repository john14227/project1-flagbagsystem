let event;
let user;
let id;

async function fetchData(){
    id = location.search;
    id = id.substr(1)
    const res = await fetch('/events/'+id);
    event = await res.json();
    const eventName = document.querySelector('#eventName');
    eventName.innerHTML = event.eventName;

    const eventDate = document.querySelector('#eventDate');
    eventDate.innerHTML = event.eventDate;

    const eventStartTime = document.querySelector('#eventStartTime');
    eventStartTime.innerHTML = event.eventStartTime;

    const eventEndTime = document.querySelector('#eventEndTime');
    eventEndTime.innerHTML = event.eventEndTime;
    
}


async function createRegisterEventFetchAsync(eventRegister){
    const res = await fetch('/event-register',{
        method: 'POST',
        headers:{
            "Content-Type":"application/json"
        },
        body: JSON.stringify(eventRegister)
    });
    const result = await res.json();
    // document.querySelector('#result').append(JSON.stringify(result))
}

document.querySelector('#create-register-form')
        .addEventListener('submit',function(eventRegister){
            eventRegister.preventDefault();
    
            const form = this;
            const formData = {
                eventId: event.id,
                userId: user.id
            };

            for(let input of form){
                if(!['submit'].includes(input.type)){
                    formData[input.name] = input.value;
                }
            }
            createRegisterEventFetchAsync(formData);
            location.href="latest-events.html"
        });


window.onload= async ()=> {
    const res = await fetch('/users/currentUser', {
        method: "GET"
    });
    user = await res.json()
    await fetchData();
}

