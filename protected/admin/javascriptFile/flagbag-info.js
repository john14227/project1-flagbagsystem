let user;
let flagbags;

async function fetchData(){
    const res = await fetch('/admin-api/get-all-flagbags');
    flagbags = await res.json();
    populateData(flagbags);
}

function populateData(flagbags){
    const all_flagbags = document.querySelector('#all_flagbags');
    all_flagbags.innerHTML = "";
    for(let flagbag of flagbags){
        console.log(flagbag);
        all_flagbags.innerHTML += `
        <tr>
            <td>${flagbag.QR_code}</td>
            <td>${flagbag.bag_status}</td>
            <td>${flagbag.collect_point_id}</td>
            <td>${flagbag.volunteer_id}</td>
            <td>${flagbag.organization_info_id}</td>
            <td>${flagbag.event_id}</td>
            <td>
                <!--<button id="myBtn2" onclick="location.href ='/admin-api/update-user.html?${user.id}'">Edit</button>-->
            </td>
        </tr>
        `
    }
}

window.onload = async ()=>{
    const res = await fetch('/users/currentUser', {
        method: "GET"
    });
    user = await res.json()
    await fetchData();
}