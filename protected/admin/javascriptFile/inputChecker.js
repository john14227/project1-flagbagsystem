document.querySelector('#submit').disabled = true;

function usernameChecker(){
    let username = document.querySelector('#username').value;
    // if (/[A-Za-z]/g.test(username) && /[0-9]/g.test(username) && username.length >= 6 && username.length < 20){
    //     if (/[^A-Za-z0-9]/g.test(username)){
    //         document.querySelector('#hint-name').innerHTML = "名稱無效!";
    //         document.querySelector('#submit').disabled = true;
    //     }
    //     else{
    //         document.querySelector('#hint-name').innerHTML = "";
    //         document.querySelector('#submit').disabled = false;
    //     }
    // } else{
    //     document.querySelector('#hint-name').innerHTML = "名稱無效!";
    //     document.querySelector('#submit').disabled = true;
    // }

    const result = /[A-Za-z]/g.test(username) && /[0-9]/g.test(username) && username.length >= 6 && username.length < 20
    commonChecker(username, '#hint-name', "名稱無效!", result)
}

function passwordChecker(){
    let password = document.querySelector('#password').value;
    // if (/[A-Za-z]/g.test(password) && /[0-9]/g.test(password)){
    //     if (/[^A-Za-z0-9]/g.test(password)){
    //         document.querySelector('#hint-password').innerHTML = "密碼無效!密碼必須為英數字且長度至少為8位(不含空格)!";
    //         document.querySelector('#submit').disabled = true;
    //     }else{
    //         document.querySelector('#hint-password').innerHTML = "";
    //         document.querySelector('#submit').disabled = false;

    //     }
    // } else{
    //     document.querySelector('#hint-password').innerHTML = "密碼無效!密碼必須為英數字且長度至少為8位(不含空格)";
    //     document.querySelector('#submit').disabled = false;
    // }

    const result =/[A-Za-z]/g.test(password) && /[0-9]/g.test(password)
    commonChecker(password, '#hint-password', "密碼無效!密碼必須為英數字且長度至少為8位(不含空格)!", result)
}

 
function commonChecker(input, hintId, hint, cond){
    if (cond){
        if (/[^A-Za-z0-9]/g.test(input)){
            document.querySelector(hintId).innerHTML = hint;
            document.querySelector('#submit').disabled = true;
        }else{
            document.querySelector(hintId).innerHTML = "";
            document.querySelector('#submit').disabled = false;

        }
    } else{
        document.querySelector(hintId).innerHTML = hint;
        document.querySelector('#submit').disabled = false;
    }
}

function secondPasswordChecker(){
    let password = document.querySelector('#password').value;
    let password2 = document.querySelector('#password2').value;
    if (!(password == password2)){
        document.querySelector('#hint-password2').innerHTML = "確認密碼不相同!";
        document.querySelector('#submit').disabled = true;
    } else {
        document.querySelector('#hint-password2').innerHTML = "";
        document.querySelector('#submit').disabled = false;
   }
}


function organizationNameChecker(){
    let organizationName = document.querySelector('#organization').value;
    if (/\S/g.test(organizationName) && organizationName.length > 1) {
        document.querySelector('#hint-organizationName').innerHTML = "";
        document.querySelector('#submit').disabled = false;
    } else {
        document.querySelector('#hint-organizationName').innerHTML = "名稱不能為空";
        document.querySelector('#submit').disabled = true;
    }
}
    
function emailChecker(){
    let email = document.querySelector('#email').value;
    if (/^[0-9a-zA-Z]+(?:[\_\.\-][a-z0-9\-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\.[a-zA-Z]+$/i.test(email)){
        document.querySelector('#hint-email').innerHTML = "";
        document.querySelector('#submit').disabled = false;
    } else {
        document.querySelector('#hint-email').innerHTML = "電郵格式無效!";
        document.querySelector('#submit').disabled = true;
    }        
}

function secondEmailChecker(){
    let email = document.querySelector('#email').value;
    let email2 = document.querySelector('#email2').value;
    if (!(email == email2)){
        document.querySelector('#hint-email2').innerHTML = "輸入電郵不相同!!";
        document.querySelector('#submit').disabled = true;
    } else {
        document.querySelector('#hint-email2').innerHTML = "";
        document.querySelector('#submit').disabled = false;
    }
}

function phoneChecker(){
    let phone = document.querySelector('#telephone').value;
    if (/[0-9]/g.test(phone) && !/[^0-9]/.test(phone) && phone.length == 8){
        document.querySelector('#hint-phone').innerHTML = "";
        document.querySelector('#submit').disabled = false;
    } else {
        document.querySelector('#hint-phone').innerHTML = "電話格式不符合要求!";
        document.querySelector('#submit').disabled = true;
    }
}

function contactPersonChecker(){
    let contactPerson = document.querySelector('#contact_person').value;
    if (/[a-zA-Z]/g.test(contactPerson)  || (/[\u4e00-\u9fa5]/.test(contactPerson))){
        if (/[^A-Za-z\u4e00-\u9fa5\s]/g.test(contactPerson)){
            document.querySelector('#hint-contactPerson').innerHTML = "只接受中英字輸入!";
            document.querySelector('#submit').disabled = true;
        } else{
            document.querySelector('#hint-contactPerson').innerHTML = "";
            document.querySelector('#submit').disabled = false;
        }
    } else {
        document.querySelector('#hint-contactPerson').innerHTML = "只接受中英字輸入!";
        document.querySelector('#submit').disabled = true;
    }
}

