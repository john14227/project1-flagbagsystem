async function createFlagbagFetchAsync(flagbag){
    const res = await fetch('/admin-api/create-new-flagbag/',{
        method:'POST',
        headers:{
            "Content-Type":"application/json"
        },
        body: JSON.stringify(flagbag)
    });
    const result = await res.json();
    document.querySelector('#result').append(JSON.stringify(result))
}

document.querySelector('#create-flagbag-form')
        .addEventListener('submit',function(flagbag){
            flagbag.preventDefault();
            console.log("Submit flagbag!");
            

            const form = this;
            const formData = {};

            for(let input of form){
                if(!['submit'].includes(input.type)){
                    formData[input.name] = input.value;
                }
            }
            createFlagbagFetchAsync(formData);
        });