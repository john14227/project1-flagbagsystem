let events;
let user;

async function fetchData(){
    const res = await fetch('/admin-api/get-all-events');
    events = await res.json();
    populateData(events);
}

function populateData(events){
    const events_result = document.querySelector('#event_result');
    events_result.innerHTML = "";
    for(let event of events){
        events_result.innerHTML += `
        <tr>
            <td>${event.event_name}</td>
            <td>${new Date(event.event_date).toDateString()}</td>
            <td>${event.event_start_time}</td>
            <td>${event.event_end_time}</td>
            <!--<td>
                <button id="myBtn2" onclick="location.href ='register-events.html?${event.id}'">Edit</button>
            </td>-->
        </tr>
        `
    }
}

window.onload = async ()=>{
    const res = await fetch('/users/currentUser', {
        method: "GET"
    });
    user = await res.json()
    await fetchData();
}

var modal = document.getElementById('myModal');
var btn = document.getElementById("myBtn");
var span = document.getElementsByClassName("close")[0];

// btn.onclick = function() {
//   modal.style.display = "block";
// }

// span.onclick = function() {
//   modal.style.display = "none";
// }

window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
