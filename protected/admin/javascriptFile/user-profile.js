let user;

window.onload = async () => {
    const res = await fetch('/users/currentUser',{
        method: 'GET'
    });
    user = await res.json();
    document.querySelector('#contact_person').value = user.contact_person;
    document.querySelector('#email').value = user.email;
    document.querySelector('#telephone').value = user.telephone;
}


async function updateUserInfo(info){
    const res = await fetch(`/users/currentUser/${user.id}`,{
        method:'PUT',
        headers: {
         "Content-Type": "application/json"
        },
        body: JSON.stringify(info)
    });
    await res.json();
    if (res.ok){
        await location.reload();
        const message = document.querySelector('#message');
        message.innerHTML = "";
        message.innerHTML = "修改用戶資料成功!";
    }
}

document.querySelector('#user-profile')
    .addEventListener('submit',async function(event){
        const form = this;
        const formData = {};
        event.preventDefault();
        for (let input of form){
            if (!['submit','password2','email2'].includes(input.name)){
                formData[input.name] = input.value;
            }
        }
        //alert(formData['password']);
        await updateUserInfo(formData);
});


// const updateForm = document.querySelector('#update-form');
// updateForm.setAttribute('action','')


// function filterInvalidInput(valid = false){
//     let username = document.querySelector('#username').value;
//     let password = document.querySelector('#password').value;
//     let password2 = document.querySelector('#password2').value;
//     let email = document.querySelector('#email').value;
//     let email2 = document.querySelector('#email2').value;
//     let phone = document.querySelector('#phone').value;
//     let contactPerson = document.querySelector('#contactPerson').value;
//     let submit = document.querySelector('#submit');

//     // if (/^[A-Za-z0-9]/g.test(username)){
//     //     submit.disable = true;
//     // }

//     // if (/^[0-9a-zA-Z]+(?:[\_\.\-][a-z0-9\-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\.[a-zA-Z]+$/i.test(email)){
//     //     submit.disable = true;
//     // }

//     // if (!(password == password2)){
//     //     submit.disable = true;
//     // }

//     // if (!(email == email2)){
//     //     submit.disable = true;
//     // }

//     // if (/[0-9]{8}/g.test(phone)){
//     //     submit.disable = true;
//     // }


//     // if (/^[a-zA-Z\u4e00-\u9fa5]{2,10}$/.test(contactPerson)){

//     // }
// }
