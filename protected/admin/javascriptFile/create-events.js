async function createEventFetchAsync(event){
    const res = await fetch('/events',{
        method:'POST',
        headers:{
            "Content-Type":"application/json"
        },
        body: JSON.stringify(event)
    });
    const result = await res.json();
    document.querySelector('#result').append(JSON.stringify(result))
}

document.querySelector('#create-event-form')
        .addEventListener('submit',function(event){
            event.preventDefault();
            console.log("Submit Event!");
            

            const form = this;
            const formData = {};

            for(let input of form){
                if(!['submit'].includes(input.type)){
                    formData[input.name] = input.value;
                }
            }
            createEventFetchAsync(formData);
        });