let user;
let users;

async function fetchData(){
    const res = await fetch('/admin-api/get-all-users');
    users = await res.json();
    populateData(users);
}

function populateData(users){
    const all_users = document.querySelector('#all_users');
    all_users.innerHTML = "";
    for(let user of users){
        console.log(user);
        all_users.innerHTML += `
        <tr>
            <td>${user.username}</td>
            <td>${user.telephone}</td>
            <td>${user.email}</td>
            <td>${user.user_eng_fullname}</td>
            <td>${user.user_chi_fullname}</td>
            <td>${user.contact_person}</td>
            <td>${user.organization_info_id}</td>
        </tr>
        `
    }
}

window.onload = async ()=>{
    const res = await fetch('/users/currentUser', {
        method: "GET"
    });
    user = await res.json()
    await fetchData();
}