import * as express from 'express';
import * as path from 'path';
import * as bodyParser from 'body-parser';
import * as expressSession from 'express-session';
import { UserRouter } from './routers/UserRouter';
import { UserService } from './services/UserService';
import { EventRouter } from './routers/EventRouter';
import { AdminRouter } from './routers/AdminRouter';
import { EventRegisterRouter } from './routers/EventRegisterRouter';
import { VolunteerRouter } from './routers/VolunteerRouter';
import {Validation} from './ServerSideInputChecker';
import * as cors from 'cors';
import * as Knex from 'knex'; // Import knex library
const knexConfig = require('./knexfile'); // Import knex config
const knex = Knex(knexConfig["development"]) ;
import {validationResult} from 'express-validator/check';
import { EventService } from './services/EventService';
import { EventRegisterService } from './services/EventRegisterService';
import { VolunteerService } from './services/VolunteerService';
import { FlagbagHistoryService } from './services/FlagbagHistoryService';

const app = express();
const expiresTime = new Date(Date.now() + 60 * 10 * 1000);

app.use(cors());

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.use(expressSession({
    secret: 'Flag Day',
    resave:true,
    saveUninitialized:true,
    // cookie:{httpOnly: true,
    //         expires: expiresTime}
}));

function isLoggedIn(req:express.Request,
    res:express.Response,
    next:express.NextFunction){
    if(req.session && req.session.isAuthenticated){
        next();
    } else{
        res.redirect('/login.html');
    }
}

 function isAdminLoggedIn(req:express.Request,
     res:express.Response,
     next:express.NextFunction){
         if(req.session && req.session.isAuthenticated && req.session.isAdmin){
             next();
         }else{
             res.redirect('/login.html')
         }
}


const userService = new UserService(knex);
const eventService = new EventService(knex);
const eventRegisterService = new EventRegisterService(knex);
const volunteerService = new VolunteerService(knex);
const flagbagHistoryService = new FlagbagHistoryService(knex);
const inputValidation = new Validation();

app.post('/register',new UserRouter(userService,inputValidation).registerInputValidation,async (req,res)=>{
    
    let err = validationResult(req);
    if (err.isEmpty()){
        // console.log(req.body);
        console.log('e');
        await userService.register(req,res);
        res.json({success: true})
    } else {
        console.log(err.mapped());
        res.json(err.mapped());
    }
})

app.post('/login',async(req,res)=>{
    
    await userService.login(req,res);
    
});

app.use('/users',isLoggedIn,new UserRouter(userService,inputValidation).router());

app.use('/events',isLoggedIn,new EventRouter(eventService).router());

app.use('/event-register',isLoggedIn,new EventRegisterRouter(eventRegisterService).router());

app.use('/volunteers',isLoggedIn,new VolunteerRouter(volunteerService).router());

app.use('/admin-api',isAdminLoggedIn,new AdminRouter(userService,inputValidation,eventService,volunteerService,eventRegisterService,flagbagHistoryService).router());


app.use(express.static('public'));

app.use("/user",isLoggedIn,express.static('protected/user'));

app.use("/admin",isAdminLoggedIn,express.static('protected/admin'))

app.get('/',async function(req,res,_next){
    try{
        if(req.session && req.session.isAuthenticated){
            if (req.session.isAdmin){
                res.redirect('/admin/dashboard.html');
            }else{
            res.redirect('/user/dashboard.html');
            }
        } else{
            res.redirect('/login.html');
        }
    } catch(e){
        res.status(404);
        res.end();
    }
});

app.use((_req,res)=>{
    res.sendFile(path.join(__dirname,"/public/404.html"));
});

const PORT = 7000;
app.listen(PORT,function(){
    console.log(`Listening at http://localhost:${PORT}/`);
});