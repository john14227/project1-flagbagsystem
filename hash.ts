import * as bcrypt from 'bcrypt';

const salt = 10;
export async function hash(plainPassword){
    const hashPassword = await bcrypt.hash(plainPassword, salt);
    return hashPassword;
}

export async function checkPassword(plainPassword,hashPassword){
    const match = await bcrypt.compare(plainPassword,hashPassword);
    return match;
}

