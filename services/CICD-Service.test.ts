import * as Knex from 'knex';
import { SampleService } from './CICD-Service';

const knexConfig = require('../knexfile');
const knex = Knex(knexConfig['testing']);

it('Can add and get records', async ()=>{
    const sampleService = new SampleService(knex);
    await sampleService.addRecord('hi');

    const records = await sampleService.getAllRecords();
    expect(records[0].name).toBe('hi');
})

afterAll(() =>{
    knex.destroy();
})