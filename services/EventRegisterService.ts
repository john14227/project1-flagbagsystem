// import * as path from 'path';
import { EventRegister } from './eventRegister-model';
// import { Event } from './events-model';
import * as Knex from 'knex';


export class EventRegisterService{
    constructor(private knex:Knex){
    }

    async retrieve(){
        return this.knex.select('*').from('event_registers');
        }

    async retrieveOrganizationRegister(){
        return this.knex.select('*').from('organization_event_registers');
        }

    async retrieveOrganizationRegisterById(id:number){
        return this.knex.select('*').from('organization_event_registers').where('organization_info_id',id);
        }
    
    async retrieveOrganizationRegisterByEventId(id:number){
        return this.knex.select('*').from('organization_event_registers').where('event_id',id)
        }

    async retrieveByEventRegisterId(eventRegisterId:number){
        return this.knex.select('*').from('event_registers').where('id',eventRegisterId);
        }

    async create(event_id:string,userId:string){
        const user = await this.knex.select('*').from('users').where('id',userId)
        const volunteers = await this.knex.select('*').from('volunteers')
        for(let volunteer of volunteers){
            if(volunteer.user_id == user[0].id){
                return this.knex.insert([
                    {
                    event_id: event_id,
                    volunteer_id: volunteer.id
                    }
                ]).into('event_registers');
            }else {
                const volunteerId = await this.knex.insert(
                    {
                        eng_fullname: user[0].user_eng_fullname,
                        chi_fullname: user[0].user_chi_fullname,
                        contact_person: user[0].contact_person,
                        contact_num: user[0].telephone

                    }).into('volunteers').returning('id');

                return this.knex.insert([
                    {
                    event_id: event_id,
                    volunteer_id: volunteerId[0]
                    }
                    ]).into('event_registers');
            }
        }
    }
    
    //update and delete function are available to individual users
    async update(id:number,updated:EventRegister){
        return this.knex('event_registers').update(updated).where('id',id);
    }
    
    async delete(id:number){
        return this.knex('event_registers').where('id',id).del();
    }

    async retrieveUserEvent(userId:number){
        const user = await this.knex.first('*').from('users').where('id',userId);
        console.log(user.organization_info_id);
        if (user.organization_info_id == null) {
            return this.knex.select('events.id','events.event_name', 'events.event_date', 'events.event_start_time', 'events.event_end_time').from('users')
            .innerJoin('volunteers','users.id','volunteers.user_id')
            .innerJoin('event_registers','event_registers.volunteer_id','volunteers.id')
            .innerJoin('events','events.id','event_registers.event_id')
            //.groupBy('events.id')
            .where('users.id',userId);
        } else {
            return this.knex.select('events.id','events.event_name', 'events.event_date', 'events.event_start_time', 'events.event_end_time').from('events')
            .innerJoin('organization_event_registers','organization_event_registers.event_id','events.id')
            .innerJoin('organization_info','organization_info.id','organization_event_registers.organization_info_id')
            .innerJoin('users','users.organization_info_id','organization_info.id')
            .where('users.id',userId);

        }

    }
}

