export interface CollectPoint {
    id?:number,
    name:string,
    address:string
}