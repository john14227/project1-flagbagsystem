import {UserService} from './UserService'
import {hash} from '../hash'
import * as Knex from 'knex';
const knexConfig = require('../knexfile');
const knex = Knex(knexConfig["testing"]);

//jest.mock('./UserService');

beforeEach(async()=>{
    await knex.migrate.rollback();
    await knex.migrate.latest();
    await knex.seed.run();
});

describe('Test user serivces CRUD',()=>{
    xtest('Get',async()=>{
        const userService = new UserService(knex);

        //const userServiceSpy = jest.spyOn(userService,'retrieve');
        expect(await userService.retrieve('paul5050')).toEqual([{
            id: 1,
            username: 'paul5050',
            password: 'paul5050',
            email: 'paul5050@gmail.com',
            organization: null,
            contact_person: null,
            telephone: '96325874'
        }]);

        expect(await userService.retrieve('jason5678')).toEqual([]);

    });

    xtest('Post',async ()=>{
        const userService = new UserService(knex);
        const john = {
            username: 'john9999',
            password: 'john9999',
            email: 'john9999@gmail.com',
            organization: null,
            contact_person: null,
            telephone: '95744283'
        };
        await userService.create(john);
        //const userServiceSpy = jest.spyOn(userService,'retrieve');
        expect(await userService.retrieve('john9999')).toEqual([{
            id: 4,
            username: 'john9999',
            password: 'john9999',
            email: 'john9999@gmail.com',
            organization: null,
            contact_person: null,
            telephone: '95744283'
        }]);
    });

    xtest('Put',async ()=>{
        const userService = new UserService(knex);
        const userList = await userService.retrieve('paul5050');
        const paul = userList[0];
        paul.telephone = '31258426';
        await userService.update(paul.id,paul);
        //const userServiceSpy = jest.spyOn(userService,'retrieve');

        expect(await userService.retrieve('paul5050')).toEqual([{
            id: 1,
            username: 'paul5050',
            password: paul.password,
            email: 'paul5050@gmail.com',
            organization: null,
            contact_person: null,
            telephone: '31258426'
        }]);
    });

    xtest('Delete', async()=>{
        const userService = new UserService(knex);
        const userList = await userService.retrieve('mary1970');
        const mary = userList[0];
        await userService.delete(mary.id);
        //const userServiceSpy = jest.spyOn(userService,'retrieve');
        expect(await userService.retrieve('mary1970')).toEqual([]);
    });

    test('login',async()=>{
        const userService = new UserService(knex);
        const req = {username: 'peter', password: '1234'}
        await userService.login('peter',hash(1234));
    })
})

afterAll(()=>{
  
    knex.destroy();
})