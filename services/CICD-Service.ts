import * as Knex from 'knex'

export class SampleService {
    constructor(private knex:Knex){

    }

    addRecord = async (name:string)=>{
        await this.knex('sample').insert({
            name:name
        })
    }

    getAllRecords = async() => {
        return await this.knex.select('*').from('sample');
    }
}