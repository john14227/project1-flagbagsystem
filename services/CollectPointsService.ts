import * as Knex from 'knex';
import{ CollectPoint } from './collectPoints-model';

export class CollectPointService{
    constructor(private knex:Knex){

    }

    async retrieve(){
            return this.knex.select('*').from('collect_points');
        }

    async retrieveByName(name){
            return this.knex.select('*').from('collect_points').where('name',name);
        }

    async retrieveSinglePoint(id){
            return this.knex.select('*').from('collect_points').where('id',id);  
        }

    async create(newPoint:CollectPoint){
            if(typeof newPoint.name === 'undefined' ||
                typeof newPoint.address === 'undefined'){
                throw new Error('Collect point is not complete');
            }
            return this.knex.insert(newPoint).into('collect_points');
        }

    async update(id:number,updated:CollectPoint){
            return this.knex('collect_points').update(updated).where('id',id);
        }
}