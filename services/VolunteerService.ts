import * as Knex from 'knex';
import { Volunteer } from './volunteer-model';
// const knexConfig = require('../knexfile');
// const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])

export class VolunteerService{
    constructor (private knex:Knex){

    }
    async retrieve(userId,eventId){
        const user = await this.knex.first('organization_info_id').from('users').where('id',userId);

        const volunteerList =  await this.knex.select('flagbags.QR_code','volunteers.eng_fullname','volunteers.chi_fullname','volunteers.contact_person','volunteers.contact_num').from('volunteers')
        .innerJoin('users','users.id','volunteers.user_id')
        .innerJoin('organization_info','organization_info.id','users.organization_info_id')
        .innerJoin('organization_event_registers','organization_info.id','organization_event_registers.organization_info_id')
        .innerJoin('events','events.id','organization_event_registers.event_id')
        .innerJoin('flagbags','volunteers.id','flagbags.volunteer_id')
        .innerJoin('flagbag_history','flagbags.id','flagbag_history.flagbag_id')
        .where('users.id' , userId)
        .andWhere('events.id',eventId)
        .andWhere('volunteers.isdeleted', false);

        const num =  await this.knex.first('organization_event_registers.volunteer_num').from('users')
        .innerJoin('organization_info','organization_info.id','users.organization_info_id')
        .innerJoin('organization_event_registers','organization_info.id','organization_event_registers.organization_info_id')
        .innerJoin('events','events.id','organization_event_registers.event_id')
        .where('users.id' , userId)
        .andWhere('events.id',eventId);

        volunteerList.forEach(volunteer => {
            const keys = Object.keys(volunteer);
            keys.forEach(key => {
                if(volunteer[key] == "${empty}")
                    volunteer[key] = "";
            });
        });

        return {volunteerList: volunteerList,
                volunteerNum: num.volunteer_num,
                organizationId: user.organization_info_id};
    }

    async update(userId,organizationId, eventId, volunteerList){
        const VolunteersId = [];
        console.log(volunteerList);
        await this.knex.transaction(async (trx: Knex.Transaction) => {
            let clearVolunteerData;

            clearVolunteerData = {};
            clearVolunteerData['chi_fullname'] = '${empty}';
            clearVolunteerData['eng_fullname'] = '${empty}';
            clearVolunteerData['contact_person'] = '${empty}';
            clearVolunteerData['contact_num'] = '${empty}';

            let flagBags = await trx.select('flagbags.id','QR_code','flagbags.volunteer_id').from('flagbags')
            //.innerJoin('volunteers','volunteers.id','flagbags.volunteer_id')
            //.innerJoin('users','users.id','volunteers.user_id')        
            .where('event_id',eventId)
            .andWhere('organization_info_id',organizationId);

            if (volunteerList.length > 0){
                for (let flagBag of flagBags) {
                    for (let volunteer of volunteerList){
                        //console.log("flagBag.QR_code:",flagBag.QR_code, "volunteer.QR_code", volunteer.QR_code);
                        if (flagBag.QR_code == volunteer.QR_code){
                            //console.log("flagBag:",flagBag.QR_code,"volunteer:",volunteer.QR_code);
                            delete volunteer.QR_code;
                            const keys = Object.keys(volunteer);
                            keys.forEach(key => {
                                if (volunteer[key] == ""){
                                    volunteer[key] = "${empty}";
                                }
                            });
                            //console.log("flagBag.QR_code:",flagBag.QR_code, "volunteer.QR_code", volunteer.QR_code);
                            volunteer.id = flagBag.volunteer_id;
                            VolunteersId.push(volunteer.id);
                            //await this.knex('volunteers').update(volunteer).where('volunteers.id',flagBag.volunteer_id);
                            flagBag.isUpdated = true;
                            break;
                        }
                    }
                }
                //console.log(volunteerList);

                const chunkSize = 50;
                await trx.schema.createTable(`update_volunteers_${userId}_${eventId}`,(table)=>{
                    table.string('eng_fullname').notNullable();
                    table.string('chi_fullname').notNullable();
                    table.string('contact_person').notNullable();
                    table.string('contact_num').notNullable();
                    table.integer('user_id').unsigned();
                    table.foreign('user_id').references('users.id');
                    table.integer('id').unique();
                    table.boolean('isdeleted').notNullable().defaultTo('false')
                    table.timestamps(false,true);
                });
        
                await trx.batchInsert(`update_volunteers_${userId}_${eventId}`,volunteerList,chunkSize);
        
                await trx.raw(`
                                UPDATE volunteers 
                                    SET chi_fullname = update_volunteers_${userId}_${eventId}.chi_fullname,
                                        eng_fullname = update_volunteers_${userId}_${eventId}.eng_fullname,
                                        contact_person = update_volunteers_${userId}_${eventId}.contact_person,
                                        contact_num = update_volunteers_${userId}_${eventId}.contact_num
                                FROM update_volunteers_${userId}_${eventId} 
                                    WHERE volunteers.id = update_volunteers_${userId}_${eventId}.id`);
        
                await trx.schema.dropTable(`update_volunteers_${userId}_${eventId}`);


                // const remainedVolunteers = [];
                // for (let flagBag of flagBags){
                //     if (flagBag.isUpdated == undefined){
                //         remainedVolunteers.push(flagBag.volunteer_id);
                //     }
                // }
                // if (remainedVolunteers.length > 0){
                //     await trx('volunteers').update(clearVolunteerData).whereIn('volunteers.id',remainedVolunteers);
                // }
            }
            // } else {
            //     console.log(flagBags);
            //     const remainedVolunteers = [];
            //     for (let flagBag of flagBags){
            //         remainedVolunteers.push(flagBag.volunteer_id);
            //     }
            //     await trx('volunteers').update(clearVolunteerData).whereIn('volunteers.id',remainedVolunteers);
            // }
        });
        
        return VolunteersId;

        // for (let flagBag of flagBags){
        //     if(flagBag.isUpdated != true){
        //         await this.knex('volunteers').update(clearVolunteerData).where('volunteers.id',flagBag.volunteer_id);
        //     }
        // }
    }    
    

    async delete(userId,eventId,organizationId){

        await this.knex.transaction(async(trx: Knex.Transaction) =>{
            let clearVolunteerData;
            clearVolunteerData = {};
            clearVolunteerData['chi_fullname'] = '${empty}';
            clearVolunteerData['eng_fullname'] = '${empty}';
            clearVolunteerData['contact_person'] = '${empty}';
            clearVolunteerData['contact_num'] = '${empty}';    
    
            
            const flagBags = await trx.select('flagbags.volunteer_id').from('flagbags')
            //.innerJoin('volunteers','volunteers.id','flagbags.volunteer_id')
            //.innerJoin('users','users.id','volunteers.user_id')        
            .where('event_id',eventId)
            .where('organization_info_id',organizationId)
    
            // for (let flagBag of flagBags){
            //     await trx('volunteers').update(clearVolunteerData).where('volunteers.id',flagBag.volunteer_id);
            // }
    
            const VolunteersId = [];
            flagBags.forEach( flagBag => {
                VolunteersId.push(flagBag.volunteer_id);
            });
            
            if (VolunteersId.length > 0){
                await trx('volunteers').update(clearVolunteerData).whereIn('volunteers.id',VolunteersId);
            }

        });
    }

    async submit (userId,organizationId, eventId, volunteerList){
        await this.knex.transaction(async (trx:Knex.Transaction)=>{
            
            const chunkSize = 50;
            let user = {};

            const VolunteersId = await this.update(userId,organizationId, eventId, volunteerList);
            let volunteerListToBefilter = await this.knex.select('id','chi_fullname','eng_fullname','contact_person','contact_num').from('volunteers').whereIn('id',VolunteersId);
            console.log(VolunteersId);
            
            const userList = [];
            const volunteerEventRegistration = [];

            volunteerListToBefilter= volunteerListToBefilter.filter((volunteer)=>{
                return volunteer.chi_fullname != "${empty}"
                    && volunteer.eng_fullname != "${empty}"
                    && volunteer.contact_person != "${empty}"
                    && volunteer.contact_num != "${empty}"
            });
            console.log("print volunteerListToBefilter",volunteerListToBefilter);
            if (volunteerListToBefilter.length > 0){
                volunteerListToBefilter.forEach(volunteer => {
                    user = {};
                    user['username'] =  Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
                    user['password'] =  Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15); 
                    user['user_chi_fullname'] = volunteer.chi_fullname;
                    user['user_eng_fullname'] = volunteer.eng_fullname;
                    user['email'] = '${empty}'+`volunteer_id:${volunteer.id}`;
                    user['contact_person'] = volunteer.contact_person;
                    user['telephone'] = volunteer.contact_num;
                    user['organization_info_id'] = organizationId;
                    user['isdeleted'] = false;
                    userList.push(user);
                    volunteerEventRegistration.push({
                        event_id: eventId
                        ,volunteer_id:volunteer.id})
                });
                console.log("userList:",userList);

                await trx.batchInsert('event_registers',volunteerEventRegistration,chunkSize);
                const userIds = await trx.batchInsert('users',userList,chunkSize).returning('id');
                
                console.log("userIds",userIds);
                const users = await trx.select('*').from('users').whereIn('id',userIds);
                const registeredVolunteerId = volunteerEventRegistration.map(item => item.volunteer_id);
                const registeredVolunteers = await trx.select('*').from('volunteers').whereIn('id',registeredVolunteerId);
                console.log("registeredVolunteerId",registeredVolunteerId);


                for (let user of users){
                    for (let registeredVolunteer of registeredVolunteers){
                        console.log("user.id",user.id,"registeredVolunteers.user_id",registeredVolunteers.user_id);
                        if(user['user_chi_fullname'] == registeredVolunteer['chi_fullname']
                        && user['user_eng_fullname'] == registeredVolunteer['eng_fullname']
                        && user['contact_person'] == registeredVolunteer['contact_person']
                        && user['telephone'] == registeredVolunteer['contact_num']){
                            registeredVolunteer.user_id = user.id;
                            //break;
                        }
                    }
                }
                
                await trx.schema.createTable(`submit_volunteerlist_for_${userId}_${eventId}`,(table)=>{
                    table.string('eng_fullname').notNullable();
                    table.string('chi_fullname').notNullable();
                    table.string('contact_person').notNullable();
                    table.string('contact_num').notNullable();
                    table.integer('user_id').unsigned();
                    table.foreign('user_id').references('users.id');
                    table.integer('id').unique();
                    table.boolean('isdeleted').notNullable().defaultTo('false')
                    table.timestamps(false,true);
                });
                await trx.batchInsert(`submit_volunteerlist_for_${userId}_${eventId}`,registeredVolunteers,chunkSize);
                await trx.raw(`
                    UPDATE volunteers 
                        SET   user_id = submit_volunteerlist_for_${userId}_${eventId}.user_id                        
                    FROM submit_volunteerlist_for_${userId}_${eventId}
                        WHERE volunteers.id = submit_volunteerlist_for_${userId}_${eventId}.id`);

                await trx.schema.dropTable(`submit_volunteerlist_for_${userId}_${eventId}`);
                await trx('organization_event_registers').update({isSubmitted:true})
                .where('organization_info_id',organizationId)
                .andWhere('event_id',eventId);
            }
        });
    }

    // async retrieve(){
    //    return this.knex.select('*').from('volunteers');
        
    // }

    async retrieveById(id:number){
        return this.knex.select('*').from('volunteers').where('id',id);
    }
    
    async retrieveByUserId(userId:number){
        return this.knex.select('*').from('volunteers').where('user_id',userId);
    }

    async retrieveByContactPerson(contactPerson:string){
        return this.knex.select('*').from('volunteers').where('contact_person',contactPerson);
    }

    async retrieveByContactNum(contactNum:number){
        return this.knex.select('*').from('volunteers').where('contact_num', contactNum);
    }

    async retrieveByEngFullname(eng_fullname:string){
        return this.knex.select('*').from('volunteers').where('eng_fullname',eng_fullname);
    }

    async retrieveByChiFullname(chi_fullname:string){
        return this.knex.select('*').from('volunteers').where('chi_fullname',chi_fullname);
    }
    // async create(newVolunteer:Volunteer){

    //     return this.knex.insert(newVolunteer).into('volunteers');
    // }

    // async update(id:number,updated:Volunteer){
    //     return this.knex('volunteers').update(updated).where('id',id);
    // }

    // async delete(id:number){
    //     return this.knex('volunteers').where('id',id).del();
    // }

}


// async function abc (knex:Knex){
//     const service =  new VolunteerService(knex);
//     console.log((await service.create(2,1)));
//     //await service.create(1)
//     await knex.destroy();

// }
// abc(knex);
