export interface Volunteer {
    id?:number,
    eng_fullname:string,
    chi_fullname:string,
    contact_person:string,
    contact_num:number,
    user_id:number
}