import * as Knex from 'knex';
import { EventRegisterService } from './EventRegisterService';
const knexfile = require('../knexfile');
const knex = Knex(knexfile["testing"]);

beforeEach(async()=>{
    await knex.migrate.rollback();
    await knex.migrate.latest();
    await knex.seed.run();
});

it('Test to get all event registers', async()=>{
    const eventRegisterService = new EventRegisterService(knex);
    const eventRegisters = await eventRegisterService.retrieve();

    expect(eventRegisters).toMatchObject([
        {
            user_id: 1,
            event_id: 1
        },
        {
            user_id: 1,
            event_id: 2
        },
        {
            user_id: 2,
            event_id: 2
        }
    ])
});

// it('Test to create new eventRegister', async()=>{
//     const eventRegisterService = new EventRegisterService(knex);
//     await eventRegisterService.create({event_id: 1},{} as any);
//     const eventRegisters = await eventRegisterService.retrieve();
//     expect(eventRegisters.find(function(eventRegister){
//         return eventRegister.user_id == 2 && eventRegister.event_id == 1;
//     })).toMatchObject(
//     {
//         volunteer_id: 2,
//         event_id: 1
//     })
// });

it('Test to create null eventRegister',async()=>{
    const eventRegisterService = new EventRegisterService(knex);
    let err;
    try {
        await eventRegisterService.create({} as any,{} as any); 
    } catch(e){
        err = e;
    }
    expect(err).toEqual(new Error('Event Register is not complete'))
});

it('Test to update event register', async()=>{
    const eventRegisterService = new EventRegisterService(knex);
    await eventRegisterService.update(1,
        {
        user_id: 2,
        event_id: 1
        }
    );
    const eventRegisters = await eventRegisterService.retrieve();
    expect(eventRegisters.find(function(eventRegister){
        return eventRegister.id == 1;
    })).toMatchObject(
        {
            user_id: 2,
            event_id: 1
        }
    )

});

// it('Test to delete event register', async()=>{
//     const eventRegisterService = new EventRegisterService(knex);
//     await eventRegisterService.create({
//         user_id: 2,
//         event_id: 1
//     });
//     await eventRegisterService.delete(4);
//     const eventRegister = await eventRegisterService.retrieveByEventRegisterId(4);
//     expect(eventRegister).toEqual([])
// });

it('Test to retrieve User\'s Event', async()=>{
    const eventRegisterService  = new EventRegisterService(knex);
    const eventRegisterId1 = await eventRegisterService.retrieveUserEvent(1)
    expect(eventRegisterId1).toMatchObject([
        {
        event_end_time: "18:00:00",
        event_name: "Happy Flag Day",
        event_start_time: "10:00:00"
        },
        {
        event_end_time: "15:00:00",
        event_name: "Testing1",
        event_start_time: "08:00:00"
        }
    ])
});

afterAll(()=>{
    knex.destroy();
});