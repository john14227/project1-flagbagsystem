
import * as Knex from 'knex';
import  {hash,checkPassword} from '../hash'
import {Request,Response} from 'express'
const knexConfig = require('../knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])

export class UserService{
    constructor(private knex:Knex){
    }

    async retrieve(username){
        return await this.knex.select('*').from('users').where('username',username);
    }

    async retrieveAllUsers() {
        return await this.knex.select('*').from('users').where('isdeleted', false);
    }

    async create(newUser) {
        return await this.knex.transaction(async(t: Knex.Transaction) => {
            const userId = await t.insert(newUser).into('users').returning('id');
            return await t.insert({
                user_id: userId[0],
                role_id: 3
            }).into('user_roles')
        })
    }

    async update(id, body) {
        const a = {
            ...body,
            password: await hash(body.password)
        }
        body['password'] = await hash(body.password);
        await this.knex('users').update(a).where('id', '=', id);
    }

    async delete(id) {
        await this.knex('users').where('id', id).update({ isdeleted: true });
    }

    async login(req,res){
        const {username,password} = req.body;
        let result:boolean = false;
        const user = await this.knex.first('users.id','roles.name as roleName','users.username','users.password')
        .from('users')
        .innerJoin('user_roles','user_roles.user_id','users.id')
        .innerJoin('roles','roles.id','user_roles.role_id')
        .where('username', username);

        if (user) {
            result = await checkPassword(password, user.password)
        }
        if(!user || !result){
            res.status(401)
            res.json({msg:"Incorrect username and password"});
        } else {
            const isAdmin = user.roleName === "admin";
            req.session.isAdmin = isAdmin;
            req.session.username = user.username;
            req.session.userId = user.id;
            req.session.isAuthenticated = true;
            res.json({is_login: true, is_admin: isAdmin});
        }
   }

    async register(req, res) {
        await this.knex.transaction(async(trx: Knex.Transaction)=>{
            const { username, password, telephone, email, organization_name, organization_address, organizationBR, contact_person, user_eng_fullname, registertype } = req.body;
            console.log(req.body);
    
            const newUser_userinfopart = {
                "username": username,
                "password": await hash(password),
                "email": email,
                "telephone": telephone,
                "contact_person": user_eng_fullname,
                "user_eng_fullname": user_eng_fullname,
                "user_chi_fullname": contact_person,
            }
    
            if (registertype == 'individual') {
                
                await this.create(newUser_userinfopart);
            }else if (registertype == 'organization') {
                    const newUser_organizationinfopart = {
    
                        "organization_name": organization_name,
                        "organization_address": organization_address,
                        "contact_num": telephone,
                        "contact_person": user_eng_fullname,
                        "BR_num": organizationBR
                    }
                    // console.log("newUser", newUser);
                    const organID = await this.knex.insert(newUser_organizationinfopart).into('organization_info').returning('id');
    
                    const newUser_userinfopart_new = {
                        ...newUser_userinfopart,
                        'organization_info_id': organID[0]
                    }
                    const userId = await this.knex.insert(newUser_userinfopart_new).into('users').returning('id');
                    await this.knex.insert({
                        user_id: userId[0],
                        role_id: 4
                    }).into('user_roles')
    
                }
        });
        }


    async logout(req: Request, res) {

        if (req.session) {
            req.session.destroy(function (err) {
                if (err) {
                    return err;
                } else {
                    return res.redirect('/');
                }
            });
        }
    }

}