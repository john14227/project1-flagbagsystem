import * as Knex from 'knex';
import { FlagbagHistory } from './flagbagHistory-model';

export class FlagbagHistoryService{
    constructor(private knex:Knex){

    }

    async retrieve(){
        return this.knex.select('*').from('flagbag_history');
    }

    async retrieveById(id:number){
        return this.knex.select('*').from('flagbag_history').where('id',id);
    }

    async retrieveByBagState(bag_state:string){
         return this.knex.select('*').from('flagbag_history').where('bag_state',bag_state);
    }
 
    async retrieveByCollectPointId(collect_point_id:number){
         return this.knex.select('*').from('flagbag_history').where('collect_point_id',collect_point_id);
    }
 
    async retrieveByVolunteerId(volunteer_id:number){
        return this.knex.select('*').from('flagbag_history').where('volunteer_id', volunteer_id);
    }

    async retrieveByOrganizationInfoId(organization_info_id:number){
        return this.knex.select('*').from('flagbag_history').where('organization_info_id',organization_info_id);
    }
 
    async retrieveByFlagbagId(flagbag_id:string){
        return this.knex.select('*').from('flagbag_history').where('flagbag_id',flagbag_id);
    }
    async create(newFlagbagHistory:FlagbagHistory){
        return this.knex.insert(newFlagbagHistory).into('flagbag_history');
    }

    async update(id:number,updated:FlagbagHistory){
        return this.knex('flagbag_history').update(updated).where('id',id);
    }

    async delete(id:number){
        return this.knex('flagbag_history').where('id',id).del();
    }
 
 }