// import * as jsonfile from 'jsonfile';
import { Event } from './events-model';
// import { EventRegister } from './eventRegister-model';
// import * as path from 'path';
import * as Knex from 'knex';

// const EVENT_JSON = path.join(__dirname,'./events.json');
// const EVENT_REGISTER_JSON = path.join(__dirname,'./eventRegister.json');

// export class EventService{

//     private EventIdCounter: number;

//     constructor(private knex:knex){
//         jsonfile.readFile(EVENT_JSON)
//             .then((data:Event[])=>{
//                 this.EventIdCounter = data.length;
//             });
//     }

//     async create(newEvent:Event){
//         const events:Event[] = await jsonfile.readFile(EVENT_JSON);
//         newEvent.id = this.EventIdCounter++;
//         events.push(newEvent);
//         await jsonfile.writeFile(EVENT_JSON,events,{ spaces: 2 })
//     }

//     async retrieve():Promise<Event[]>{
//         return jsonfile.readFile(EVENT_JSON);
//     }

    // async retrieveUnregisterObject(userId:number){
    //     const eventRegisters:EventRegister[] = await jsonfile.readFile(EVENT_REGISTER_JSON);
    //     const events:Event[] = await jsonfile.readFile(EVENT_JSON);
    //     const event = events.filter(function(event) {
    //         for (let eventRegister of eventRegisters){
    //             if ((event.id == eventRegister.eventId) && (eventRegister.userId == userId)){
    //                 return false;
    //             }
    //         }
    //         return true;
    //     })
    //     return event;   
    // }

//     async retrieveSingleObject(id:number){
//         const events:Event[] = await jsonfile.readFile(EVENT_JSON);
//         const event = events.find((event)=>event.id === id);
//         return event;
//     }

//     async update(id:number,updated:Event){
//         const events:Event[] = await jsonfile.readFile(EVENT_JSON);
//         const event = events.find((event)=>event.id === id);
//         Object.assign(event,updated);
//         await jsonfile.writeFile(EVENT_JSON,events,{ spaces: 2 })
//     }

//     async delete(id:number){
//         let events:Event[] = await jsonfile.readFile(EVENT_JSON);
//         events = events.filter((event)=>event.id !== id);
//         await jsonfile.writeFile(EVENT_JSON,events,{ spaces: 2 })
//     }
// }


export class EventService{
    constructor(private knex:Knex){

    }

    async retrieve(){
        return this.knex.select('*').from('events');
    }

    async create(newEvent:Event){
        if(typeof newEvent.event_date === 'undefined' || 
            typeof newEvent.event_end_time === 'undefined' ||
            typeof newEvent.event_date === 'undefined' ||
            typeof newEvent.event_start_time === 'undefined'){
                throw new Error('Event is not complete');
            }
        return this.knex.insert(newEvent).into('events');
    }

    async update(id:number,updated:Event){
        return this.knex('events').update(updated).where('id',id);
    }

    async delete(id){
        return this.knex('events').where('id',id).del();
    }

    async retrieveSingleObject(id:number){
        return this.knex.select('*').from('events')
                                .where('id', id);
    }

    async retrieveUnregisterObject(userId:number){
        return this.knex.select('events.id','events.event_name','events.event_date','events.event_start_time','events.event_end_time')
        .from('events').whereNotIn('id',function(){
            this.select('events.id').from('events')
            .innerJoin('event_registers','events.id','event_registers.event_id')
            .innerJoin('volunteers','event_registers.volunteer_id','volunteers.id')
            .innerJoin('users','users.id','volunteers.user_id')
            .where('users.id',userId)
            }
        )
    }
}