import * as Knex from 'knex';
import { EventService } from './EventService';
const knexfile = require('../knexfile');
const knex = Knex(knexfile["testing"]);


beforeEach(async ()=>{
    await knex.migrate.rollback();
    await knex.migrate.latest();
    await knex.seed.run()
});
xit('Test get all events', async()=>{
    const eventService = new EventService(knex);
    const events = await eventService.retrieve();

    expect(events).toMatchObject([
        {
            event_name: 'Happy Flag Day',
            event_date: new Date('2019-01-18T16:00:00.000Z'),
            //Postgres timezone problem, automated use UTC timezone
            event_start_time: '10:00:00',
            event_end_time: '18:00:00'
        },
        {
            event_name: 'Testing1',
            event_date: new Date('2019-03-19T16:00:00.000Z'),
            event_start_time: '08:00:00',
            event_end_time: '15:00:00'
        }
    ])
});

xit('Test to create new event', async()=>{
    const eventService = new EventService(knex);
    await eventService.create({
        event_name: 'Flag Day Test 2',
        event_date: '2020-01-01',
        event_start_time: '08:00:00',
        event_end_time: '20:00:00'
    });
    const events = await eventService.retrieve();
    expect(events.find(function(event){
        return event.event_name == 'Flag Day Test 2';
    })).toMatchObject(
        {
            event_name: 'Flag Day Test 2',
            event_date: new Date('2019-12-31T16:00:00.000Z'),
            //Postgres timezone problem, automated use UTC timezone
            event_start_time: '08:00:00',
            event_end_time: '20:00:00'
        }
    )
});

xit('Test to create null event',async()=>{
    const eventService = new EventService(knex);
    let err;
    try {
        await eventService.create({} as any); 
    } catch(e){
        err = e;
    }
    expect(err).toEqual(new Error('Event is not complete'))
});

xit('Test to update event',async()=>{
    const eventService = new EventService(knex);
    await eventService.update(1,
        {
        event_name: 'Happy Flag Day Edited',
        event_date: '2020-01-01',
        event_start_time: '11:00:00',
        event_end_time: '21:00:00'
        }
    );
        
    const events = await eventService.retrieve();

    expect(events.find(function(event){
        return event.event_name == 'Happy Flag Day Edited';
    })).toMatchObject(
        {
            event_name: 'Happy Flag Day Edited',
            event_date: new Date('2019-12-31T16:00:00.000Z'),
            //Postgres timezone problem, automated use UTC timezone
            event_start_time: '11:00:00',
            event_end_time: '21:00:00'
        }
    )
});

xit('Test to delete event', async()=>{
    const eventService = new EventService(knex);
    await eventService.create({
        event_name: 'Testing 3',
        event_date: '2020-03-02',
        event_start_time: '08:00:00',
        event_end_time: '21:00:00'
    });
    await eventService.delete(3)
    const event = await eventService.retrieveSingleObject(3);
    expect(event).toEqual([])
});

xit('Test to retrieve single event by event_id', async()=>{
    const eventService = new EventService(knex);
    const event = await eventService.retrieveSingleObject(1);

    expect(event).toMatchObject([
            {
                event_name: 'Happy Flag Day',
                event_date: new Date('2019-01-18T16:00:00.000Z'),
                //Postgres timezone problem, automated use UTC timezone
                event_start_time: '10:00:00',
                event_end_time: '18:00:00'
            }])
    
});

it('Test to retrieve unregister event by user_id', async()=>{
    const eventService = new EventService(knex);
    const unregister_event = await eventService.retrieveUnregisterObject(1);

    expect(unregister_event).toMatchObject([
        {
            event_name: 'Testing2',
            event_start_time: '08:00:00'
        },
        {
            event_name: 'Testing3',
            event_end_time: '15:00:00'
        }
    ]);
});

afterAll(()=>{
    knex.destroy();
});