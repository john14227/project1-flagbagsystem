export interface User {
    id?:number,
    username:string,
    password:string,
    organization_name?:string,
    organization_address?:string,
    organizationBR?:number,
    email:string,
    user_eng_fullname:string,
    user_chi_fullname?:string,
    contact_person: string,
    telephone:number,
    organization_info_id:number
}