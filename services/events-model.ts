export interface Event {
    id?:number,
    event_name:string,
    event_date:string,
    event_start_time:string,
    event_end_time:string
}