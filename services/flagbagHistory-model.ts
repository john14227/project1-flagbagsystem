export interface FlagbagHistory {
    id?:number,
    bag_state:string,
    collect_point_id:string,
    volunteer_id:number,
    organization_info_id:number,
    flagbag_id:number
}