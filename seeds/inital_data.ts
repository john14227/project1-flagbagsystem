import {hash} from '../hash'
import * as Knex from "knex";

exports.seed = async function (knex: Knex){
    await knex.table('flagbag_history').del();
    await knex.table('user_roles').del();
    await knex.table('collect_point_leaders').del();
    await knex.table('event_registers').del();
    await knex.table('volunteers').del();
    await knex.table('users').del();
    await knex.table('roles').del();
    await knex.table('organization_event_registers').del();
    await knex.table('organization_info').del();
    await knex.table('event_collect_points').del();
    await knex.table('collect_points').del();
    await knex.table('events').del();

    const events = await knex.insert([
        {
            event_name: 'Testing0',
            event_date: '2019-01-19',
            event_start_time: '10:00',
            event_end_time: '18:00'
        },
        {
            event_name: 'Testing1',
            event_date: '2019-03-20',
            event_start_time: '08:00',
            event_end_time: '15:00'
        },
        {
            event_name: 'Testing2',
            event_date: '2019-03-22',
            event_start_time: '08:00',
            event_end_time: '15:00'
        },        {
            event_name: 'Testing3',
            event_date: '2019-03-25',
            event_start_time: '08:00',
            event_end_time: '15:00'
        }
    ]).into('events').returning('id');

    const eventId1 = events[0];
    const eventId2 = events[1];
    const eventId3 = events[2];
    const eventId4 = events[3];

    const collect_points = await knex.insert([
        {
            name: 'AAA batch 001',
            address: 'AAA STREET AA ROAD'
        },
        {
            name: 'BBB batch 001',
            address: 'BBB STREET CENTRAL'
        },
        {
            name: 'CCC batch 00',
            address: 'CCC STREET CENTRAL'
        }

    ]).into('collect_points').returning('id');

    const collect_pointID1 = collect_points[0];
    const collect_pointID2 = collect_points[1];
    const collect_pointID3 = collect_points[2];

    await knex.batchInsert('event_collect_points',[
        {collect_points_id: collect_pointID1,event_id: eventId1},
        {collect_points_id: collect_pointID2,event_id: eventId1},
        {collect_points_id: collect_pointID2,event_id: eventId3}
    ]);

    const organization_info = await knex.insert([
        {
            organization_name: 'ABC Company',
            organization_address: 'ABD ROAD HONGKONG',
            contact_num: '22332211',
            contact_person: 'Peter Chong',
            BR_num: '00991124'
        },
        {
            organization_name: 'BBH Company',
            organization_address: 'HKG ROAD HONGKONG',
            contact_num: '22002011',
            contact_person: 'Mary Ko',
            BR_num: '90017A0'
        }

    ]).into('organization_info').returning('id');

    const organization_infoID1 = organization_info[0];
    const organization_infoID2 = organization_info[1];

    await knex.batchInsert('organization_event_registers',[
        {volunteer_num: 50, event_id: eventId1, organization_info_id: organization_infoID1},
        {volunteer_num: 30, event_id: eventId2, organization_info_id: organization_infoID2}
    ]);

    const roles = await knex.insert([
        {name: 'admin'},
        {name: 'collect_point_leader'},
        {name: 'individual'},
        {name: 'organization'}
        ]).into('roles').returning('id');
    
    const roleID1 = roles[0];
    const roleID2 = roles[1];
    const roleID3 = roles[2];
    const roleID4 = roles[3];

    const users = await knex.insert([
        {
            username: 'admin',
            password: await hash('admin'),
            telephone: '99901123',
            email: 'testing0@gmail.com',
            user_eng_fullname: 'CHAN TAI MAN',
            user_chi_fullname: '陳大文',
            contact_person:"Peter Chan"
        },
        {
            username: 'peter',
            password: await hash('1234'),
            telephone: '99901125',
            email: 'testing1@gmail.com',
            user_eng_fullname: 'CHAN SIU MEN',
            user_chi_fullname: '陳小文',
            contact_person:"Peter Chan",
            organization_info_id: organization_infoID1
        },
        {
            username: 'mary',
            password: await hash('1234'),
            telephone: '22422271',
            email: 'testing2@gmail.com',
            user_eng_fullname: 'NG KA LO',
            user_chi_fullname: '吳嘉樂',
            contact_person:"CHAN TAI WA",
            organization_info_id: organization_infoID2
        }
    ]).into('users').returning('id');

    const userID1 = users[0];
    const userID2 = users[1];
    const userID3 = users[2];



    const volunteers = await knex.insert([
            {
                eng_fullname: 'CHAN TAI MAN',
                chi_fullname: '陳大文',
                contact_person: 'CHAN TAI MAN',
                contact_num: '99901123',
                user_id: userID2
            },
            {
                eng_fullname: 'NG KA LO',
                chi_fullname: '吳嘉樂',
                contact_person: 'CHAN TAI WA',
                contact_num: '22422271',
                user_id: userID3
            },
            {
                eng_fullname: 'Li Four',
                chi_fullname: '李四',
                contact_person: 'Li Four',
                contact_num: '99213271',
                user_id: userID2 //null 
            }
        ]).into('volunteers').returning('id');
    
    const volunteerID1 = volunteers[0];
    const volunteerID2 = volunteers[1];
    const volunteerID3 = volunteers[2];

    await knex.batchInsert('event_registers',[
        {event_id: eventId1, volunteer_id: volunteerID1},
        {event_id: eventId1, volunteer_id: volunteerID3},
        {event_id: eventId2, volunteer_id: volunteerID1},
        {event_id: eventId2, volunteer_id: volunteerID2}
    ])



    await knex.batchInsert('collect_point_leaders',[
        {
            collect_point_id: collect_pointID1,
            user_id: userID2
        }
    ])

    await knex.batchInsert('user_roles',[
        {user_id: userID1, role_id: roleID1},
        {user_id: userID2, role_id: roleID3},
        {user_id: userID3, role_id: roleID4}
    ])

    // const flagbags = await knex.insert([
    //     {
    //         QR_code: 'A001',
    //         collect_point_id: collect_pointID1,
    //         event_id: eventId1,
    //         volunteer_id: volunteerID1,
    //         organization_info_id: organization_infoID1                
    //     },
    //     {
    //         QR_code: 'A002',
    //         collect_point_id: collect_pointID2,
    //         event_id: eventId1,
    //         volunteer_id: volunteerID2,
    //         organization_info_id: organization_infoID2
    //     },
    //     {
    //         QR_code: 'A003',
    //         collect_point_id: collect_pointID2,
    //         event_id: eventId1,
    //         volunteer_id: volunteerID3,
    //         organization_info_id: organization_infoID1
    //     }
    // ]).into('flagbags').returning('id');

    // const flagbag1 = flagbags[0];
    // const flagbag2 = flagbags[1];
    // const flagbag3 = flagbags[2]


    await knex.batchInsert('flagbag_history',[
        {
            QR_code: 'A001',
            bag_status: 'normal',
            collect_point_id: collect_pointID1,
            volunteer_id: volunteerID1,
            organization_info_id: organization_infoID1,
            event_id: eventId1
        },
        {
            QR_code: 'A002',
            bag_status: 'normal',
            collect_point_id: collect_pointID2,
            volunteer_id: volunteerID2,
            organization_info_id: organization_infoID1,
            event_id: eventId2
        },
        {
            QR_code: 'A003',
            bag_status: 'normal',
            collect_point_id: collect_pointID1,
            volunteer_id: volunteerID3,
            organization_info_id: organization_infoID1,
            event_id: eventId1
        }
    ])
};