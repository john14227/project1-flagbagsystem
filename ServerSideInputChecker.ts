import { check, body } from 'express-validator/check';
import { resolve } from 'bluebird';


export class Validation {
    constructor() {
    }

    username =
        check('username').custom((username) => {
            if (!(/[A-Za-z.]/.test(username) || /[0-9.]/.test(username) && username.length >= 6)) {
                return Promise.reject('名稱無效!');
            } else {
                return Promise.resolve();
            }
        });



    password =
        body('password').custom((password) => {
            if (password.length < 8 || /\s/g.test(password)) {
                return Promise.reject('密碼無效!密碼必須為英數字且長度至少為8位(不含空格)');
            } else {
                return Promise.resolve();
            }

        });


        organizationName =
        body('organization_name').custom((organization_name, { req }) => {
            const registertype = req.body.registertype;
            if (registertype == "individual") {
                return Promise.resolve();
            } else {
                if (/\S/g.test(organization_name)) {
                    return Promise.resolve();
                } else {
                    return Promise.resolve('公司/組織/團體名稱不能為空白');
                }
            }
        });
    //check('organization','名稱不能為空白').isEmpty();


    organizationAddress =
    body('organization_address').custom((organization_address, { req }) => {
        const registertype = req.body.registertype;
        if (registertype == "individual") {
            return Promise.resolve();
        } else {
            if (/\S/g.test(organization_address)) {
                return Promise.resolve();
            } else {
                return Promise.resolve('地址不能為空白');
            }
        }
    });

    organizationBR =
    body('organizationBR').custom((organizationBR, { req }) => {
        const registertype = req.body.registertype;
        if (registertype == "individual") {
            return Promise.resolve();
        } else {
            if (/[0-9]/g.test(organizationBR)) {
                return Promise.resolve();
            } else {
                return Promise.resolve('商業登記號碼格式不符合要求');
            }
        }
    });

    email =
        check('email', '電郵格式無效').isEmail();

    contactPerson =
        body('contact_person').custom((contact_person) => {
            if (/[^u4e00-\u9fa5\s]/g.test(contact_person)) {
                return Promise.reject('只接受中文字輸入!');
            } else {
                return resolve();
            }
        })

    contactPersoneng =
        body('user_eng_fullname').custom((user_eng_fullname) => {
            if (/[^A-Za-z\s]/g.test(user_eng_fullname)) {
                return Promise.reject('只接受英文字輸入!');
            } else {
                return resolve();
            }
        })

    telephone =
        check('telephone', '電話格式不符合要求!')
            .isLength({ min: 8, max: 8 })
            .isInt();
}






