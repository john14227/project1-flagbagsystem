async function getUserFetchAsync(){
    const res = await fetch('/register');
    const result = await res.json();
    document.querySelector('#result').append(JSON.stringify(result))

}

async function createUserFetchAsync(user){
    const res = await fetch('/register',{
        method: 'POST',
        headers:{
            "Content-Type":"application/json"
        },
        body: JSON.stringify(user)

    });
    const result = await res.json();
    document.querySelector('#result').append(JSON.stringify(result))

}

async function createUser(user){
    const res = await fetch('/users',{
        method:'POST',
        headers:{
            "Content-Type":"application/json"
        },
        body: JSON.stringify(user)
    });
    await res.json();
    fetchData();
}

document.querySelector('#user-register-form')
        .addEventListener('submit',async function(event){
            event.preventDefault();
            const form = this;
            const formData = {};

            for(let input of form){
                if(!['submit','reset','password2','email2'].includes(input.name)){
                    formData[input.name] = input.value;
                }
            }
            await updateUser(formData);
        });

