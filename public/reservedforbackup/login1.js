function redirect(){
    location.replace("/register.html")
}

document.querySelector('#register').addEventListener('click',()=>{

})

async function createUserFetchAsync(user){
    const res = await fetch('/login',{
        method: 'POST',
        headers:{
            "Content-Type":"application/json"
        },
        body: JSON.stringify(user)

    });
    const result = await res.json();
    const message = document.querySelector('#result');
    message.innerHTML = "";
    message.append(JSON.stringify(result));

}

//Ajax Form
document.querySelector('#user-login-form')
        .addEventListener('submit',function(event){
            console.log("Submit Event!");
            event.preventDefault();
            const form = this;
            const formData = {};

            for(let input of form){
                if(!['submit','reset'].includes(input.type)){
                    formData[input.name] = input.value;
                }
            }
            createUserFetchAsync(formData);
        });