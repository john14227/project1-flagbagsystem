/* AJAX starts here!!!!*/
async function createUserFetchAsync(user){
    const res = await fetch('/login',{
        method: 'POST',
        headers:{
            "Content-Type":"application/json"
        },
        body: JSON.stringify(user)
     });
    const result = await res.json();
    const message = document.querySelector('#message');
    if (res.status == 401){
        message.innerHTML = '';
        message.innerHTML = '用戶名稱或密碼錯誤!';

    } else if (result.is_admin){
        location.href = ('/admin/dashboard.html');

    } else if(result.is_login){
        location.href = ('/user/dashboard.html');
    }
}

//Ajax Form
document.querySelector('#user-login-form')
        .addEventListener('submit',function(event){
            console.log("Submit Event!");
            event.preventDefault();
            const form = this;
            const formData = {};

            for(let input of form){
                if(!['submit','reset'].includes(input.type)){
                    formData[input.name] = input.value;
                }
            }
            createUserFetchAsync(formData);
        });


function redirect(){
    location.href = "/register.html";
}