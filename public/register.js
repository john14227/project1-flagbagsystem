//import * as inputChecker from './inputChecker.js';


// async function getUserFetchAsync(){
//     const res = await fetch('/register');
//     const result = await res.json();
//     document.querySelector('#result').append(JSON.stringify(result))

// }

async function createUserFetchAsync(user) {

  try {
    const res = await fetch('/register', {
      method: 'POST',
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(user)
    });
    
    if (res.ok) {
      document.querySelector('#message').innerHTML = "登記成功!"
    }
  } catch (e) {
    console.log(e)
  }

  // const result = await res.json();
  // document.querySelector('#result').append(JSON.stringify(result))
}

// async function createUser(user){
//     const res = await fetch('/users',{
//         method:'POST',
//         headers:{
//             "Content-Type":"application/json"
//         },
//         body: JSON.stringify(user)
//     });
//     await res.json();
//     fetchData();
// }



document.querySelector('#user-register-form')
  .addEventListener('submit', async function (event) {
    event.preventDefault();


    const form = this;
    const formData = {};

    for (let input of form) {
      if (!['submit', 'reset', 'password2', 'email2', 'radiobtn'].includes(input.name)) {
        formData[input.name] = input.value;
      }


    }
    // const registerType = document.querySelectorAll('.choices').values;
    // for (let choice of registerType) {
    //     if (choice.check == true) {
    //         if (document.getElementById('individual-form').checked){
    //             const show = document.querySelector('#organization-form-show');
    //                 show.innerHTML = "";
    //             }
    //     }
    // }
    // console.log(formData);
    const types = document.getElementsByClassName('register');
    for (let type in types) {
      if (types[type].checked) {
        formData.registertype = types[type].value;

      }
    }

    console.log(formData);
    await createUserFetchAsync(formData);

  });

function organizationCheck() {
  if (document.getElementById('organization').checked) {
    const show = document.querySelector('#form-show');
    show.innerHTML = "";
    show.innerHTML += `
        <div class='form-group'>
                      <label class='control-label col-md-2 col-md-offset-2'>公司/團體 名稱:</label>    
                      <div class='col-md-3'>
                        <div class='form-group'>
                          <div class='col-md-11'>
                            <input id="organization_name" class='form-control' type="text" name="organization_name"
                              placeholder='公司/組織 名稱' oninput="organizationNameChecker()" required>
                            <span class = "register-hint" id="hint-organizationName"></span>
                          </div>
                        </div>
                      </div>
        
                    </div>
        
                    <div class='form-group'>
        
                      <label class='control-label col-md-2 col-md-offset-2'>公司/組織 地址:</label>
        
                      <div class='col-md-3'>
        
                        <div class='form-group'>
                          <div class='col-md-11'>
                            <input id="organization_address" class='form-control' type="text" name="organization_address" placeholder='公司/團體地址'
                              oninput="organizationAddressChecker()" required>
                            <span class = "register-hint" id="hint-organizationAddress"></span>
                          </div>
                        </div>
        
                      </div>
                    </div>
        
        
                    <div class='form-group'>
        
                      <label class='control-label col-md-2 col-md-offset-2'>商業登記號碼:</label>
        
                      <div class='col-md-3'>
        
                        <div class='form-group'>
                          <div class='col-md-11'>
                            <input id="organizationBR" name="organizationBR" class='form-control' type="text" placeholder='商業登記號碼'
                              oninput="organizationBRChecker()" required><small>首8位數字</small>
                            <span class = "register-hint" id="hint-organizationBR"></span>
                          </div>
                        </div>
        
                      </div>
                    </div>
                    <div class='form-group' id="">

                    <label class='control-label col-md-2 col-md-offset-2' for='contact-name'>公司/團體 聯絡人全名姓名:(中文)</label>
        
                    <div class='col-md-3'>
                      <div class='form-group'>
                        <div class='col-md-11'>
                          <input id="contact_person" class='form-control' type="text" name="contact_person" placeholder='聯絡人中文全名'
                            oninput="contactPersonChecker()">
                          <span class = "register-hint" id="hint-contactPerson"></span>
                        </div>
                      </div>
        
                    </div>
                  </div>
        
                `

  };
  if (document.getElementById('individual').checked) {
    const show = document.querySelector('#form-show');
    show.innerHTML = "";
    show.innerHTML += `
        <div class='form-group'>
          <label class='control-label col-md-2 col-md-offset-2'>申請人全名:(中文)</label>    
          <div class='col-md-3'>
            <div class='form-group'>
              <div class='col-md-11'>
                <input id="contact_person" class='form-control' type="text" name="contact_person"
                  placeholder='中文全名' oninput="contactPersonChecker()">
                <span class = "contactPerson-hint" id="hint-contactPerson"></span>
              </div>
            </div>
          </div>

        </div>`
  }
};



// function filterInvalidInput(){
//     let username = document.querySelector('#username').value;
//     let password = document.querySelector('#password').value;
//     let password2 = document.querySelector('#password2').value;
//     let email = document.querySelector('#email').value;
//     let email2 = document.querySelector('#email2').value;
//     let organizationName = document.querySelector('#organizationName').value;
//     let phone = document.querySelector('#phone').value;
//     let contactPerson = document.querySelector('#contactPerson').value;
//     let submit = document.querySelector('#submit');



//     if (/[A-Za-z]/g.test(username) && /[0-9]/g.test(username) && username.length > 6 && username.length < 20){
//         if (/[^A-Za-z0-9]/g.test(username)){
//             document.querySelector('#hint-name').innerHTML = "名稱無效!";
//             submit.disable = true;
//         }
//         else{
//             document.querySelector('#hint-name').innerHTML = "";
//             submit.disable = false;
//         }
//     } else{
//         document.querySelector('#hint-name').innerHTML = "名稱無效!";
//         submit.disable = true;
//     }


//     if (/[A-Za-z]/g.test(password) && /[0-9]/g.test(password)){
//         if (/[^A-Za-z0-9]/g.test(password)){
//             document.querySelector('#hint-password').innerHTML = "密碼無效!密碼必須為英數字且長度至少為8位(不含空格)!";
//             submit.disable = true;
//         }else{
//             document.querySelector('#hint-password').innerHTML = "";
//             submit.disable = false;
//         }
//     } else{
//         document.querySelector('#hint-password').innerHTML = "密碼無效!密碼必須為英數字且長度至少為8位(不含空格)";
//         submit.disable = true;
//     }

//     if (!(password == password2)){
//          document.querySelector('#hint-password2').innerHTML = "確認密碼不相同!";
//          submit.disable = true;
//      } else {
//          document.querySelector('#hint-password2').innerHTML = "";
//          submit.disable = false;
//      }

//      if (/\S/g.test(organizationName) && organizationName.length > 1) {
//         document.querySelector('#hint-organizationName').innerHTML = "";
//         submit.disable = false;
//     } else {
//         document.querySelector('#hint-organizationName').innerHTML = "名稱不能為空";
//         submit.disable = true;
//     }


//     if (/^[0-9a-zA-Z]+(?:[\_\.\-][a-z0-9\-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\.[a-zA-Z]+$/i.test(email)){
//         document.querySelector('#hint-email').innerHTML = "";
//         submit.disable = false;
//     } else {
//         document.querySelector('#hint-email').innerHTML = "電郵格式無效!";
//         submit.disable = true;
//     }

//     if (!(email == email2)){
//         document.querySelector('#hint-email2').innerHTML = "輸入電郵不相同!!";
//         submit.disable = true;
//     } else {
//         document.querySelector('#hint-email2').innerHTML = "";
//         submit.disable = false;
//     }


//     if (/[0-9]/g.test(phone) && !/[^0-9]/.test(phone) && phone.length == 8){
//         document.querySelector('#hint-phone').innerHTML = "";
//         submit.disable = false;
//     } else {
//         document.querySelector('#hint-phone').innerHTML = "電話格式不符合要求!";
//         submit.disable = true;
//     }


//     if (/[a-zA-Z]/g.test(contactPerson) || /[\u4e00-\u9fa5]/.test(contactPerson)){
//         document.querySelector('#hint-contactPerson').innerHTML = "";
//         submit.disable = false;
//     } else {
//         document.querySelector('#hint-contactPerson').innerHTML = "只接受中英字輸入!";
//         submit.disable = true;
//     }

// }