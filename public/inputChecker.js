document.querySelector('#submit').disabled = true;

function usernameChecker() {
    let username = document.querySelector('#username').value;
    if (/[A-Za-z.]/g.test(username) && /[0-9.]/g.test(username) && username.length >= 6) {
        if (/[^A-Za-z0-9.]/g.test(username)) {
            document.querySelector('#hint-name').innerHTML = "名稱無效！";
            document.querySelector('#submit').disabled = true;
        }
        else {
            document.querySelector('#hint-name').innerHTML = "";
            document.querySelector('#submit').disabled = false;
        }
    } else {
        document.querySelector('#hint-name').innerHTML = "名稱無效！";
        document.querySelector('#submit').disabled = true;
    }
}

function passwordChecker() {
    let password = document.querySelector('#password').value;
    if (/[A-Za-z.]/g.test(password) && /[0-9.]/g.test(password) && password.length >= 8) {
        if (/[^A-Za-z0-9.]/g.test(password)) {
            document.querySelector('#hint-password').innerHTML = "密碼無效！密碼必須為英數字且長度至少為8位(不含空格)！";
            document.querySelector('#submit').disabled = true;
        } else {
            document.querySelector('#hint-password').innerHTML = "";
            document.querySelector('#submit').disabled = false;

        }
    } else {
        document.querySelector('#hint-password').innerHTML = "密碼無效！密碼必須為英數字且長度至少為8位(不含空格)";
        document.querySelector('#submit').disabled = false;
    }
}

function secondPasswordChecker() {
    let password = document.querySelector('#password').value;
    let password2 = document.querySelector('#password2').value;
    if (!(password == password2)) {
        document.querySelector('#hint-password2').innerHTML = "確認密碼不相同！";
        document.querySelector('#submit').disabled = true;
    } else {
        document.querySelector('#hint-password2').innerHTML = "";
        document.querySelector('#submit').disabled = false;
    }
}


function organizationNameChecker() {
    let organizationName = document.querySelector('#organization_name').value;
    if (/\S/g.test(organizationName) && organizationName.length > 1) {
        document.querySelector('#hint-organizationName').innerHTML = "";
        document.querySelector('#submit').disabled = false;
    } else {
        document.querySelector('#hint-organizationName').innerHTML = "公司/組織/團體名稱不能為空白";
        document.querySelector('#submit').disabled = true;
    }
}

function organizationAddressChecker() {
    let organizationAddress = document.querySelector('#organization_address').value;
    if (/\S/g.test(organizationAddress) && organizationAddress.length > 1) {
        document.querySelector('#hint-organizationAddress').innerHTML = "";
        document.querySelector('#submit').disabled = false;
    } else {
        document.querySelector('#hint-organizationAddress').innerHTML = "地址不能為空白";
        document.querySelector('#submit').disabled = true;
    }
}


function organizationBRChecker() {
    let organizationBR = document.querySelector('#organizationBR').value;
    if (/[0-9]/g.test(organizationBR) && organizationBR.length == 8) {
        document.querySelector('#hint-organizationBR').innerHTML = "";
        document.querySelector('#submit').disabled = false;
    } else {
        document.querySelector('#hint-organizationBR').innerHTML = "商業登記號碼格式不符合要求";
        document.querySelector('#submit').disabled = true;
    }

}



function emailChecker() {
    let email = document.querySelector('#email').value;
    if (/^[0-9a-zA-Z]+(?:[\_\.\-][a-z0-9\-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\.[a-zA-Z]+$/i.test(email)) {
        document.querySelector('#hint-email').innerHTML = "";
        document.querySelector('#submit').disabled = false;
    } else {
        document.querySelector('#hint-email').innerHTML = "電郵格式無效！";
        document.querySelector('#submit').disabled = true;
    }
}

function secondEmailChecker() {
    let email = document.querySelector('#email').value;
    let email2 = document.querySelector('#email2').value;
    if (!(email == email2)) {
        document.querySelector('#hint-email2').innerHTML = "輸入電郵不相同！";
        document.querySelector('#submit').disabled = true;
    } else {
        document.querySelector('#hint-email2').innerHTML = "";
        document.querySelector('#submit').disabled = false;
    }
}

function contactPersonChecker() {
    let contactPerson = document.querySelector('#contact_person').value;
    if (contactPerson !== null) {
        if (/[\u4e00-\u9fa5]/.test(contactPerson)) {
            if (/[^u4e00-\u9fa5\s]/g.test(contactPerson)) {
                document.querySelector('#hint-contactPerson').innerHTML = "只接受中文字輸入！";
                document.querySelector('#submit').disabled = true;
            } else {
                document.querySelector('#hint-contactPerson').innerHTML = "";
                document.querySelector('#submit').disabled = false;
            }
        } else {
            document.querySelector('#hint-contactPerson').innerHTML = "只接受中文字輸入！";
            document.querySelector('#submit').disabled = true;
        }
    }
    if (contactPerson == "") {
    document.querySelector('#submit').disabled = false;
    }
}


function contactPersonENGChecker() {
    let contactPersonENG = document.querySelector('#user_eng_fullname').value;
    if (/[A-Za-z\s]/g.test(contactPersonENG)) {
        if (/[^A-Za-z\s]/g.test(contactPersonENG)) {
            document.querySelector('#hint-user_eng_fullname').innerHTML = "只接受英文字輸入！";
            document.querySelector('#submit').disabled = true;
        } else {
            document.querySelector('#hint-user_eng_fullname').innerHTML = "";
            document.querySelector('#submit').disabled = false;
        }
    } else {
        document.querySelector('#hint-user_eng_fullname').innerHTML = "只接受英文字輸入！";
        document.querySelector('#submit').disabled = true;
    }
}

function phoneChecker() {
    let phone = document.querySelector('#telephone').value;
    if (/[0-9]/g.test(phone) && !/[^0-9]/.test(phone) && phone.length == 8) {
        document.querySelector('#hint-phone').innerHTML = "";
        document.querySelector('#submit').disabled = false;
    } else {
        document.querySelector('#hint-phone').innerHTML = "電話格式不符合要求！";
        document.querySelector('#submit').disabled = true;
    }
}