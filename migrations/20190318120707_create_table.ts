import * as Knex from "knex";

async function events(knex:Knex){
    const hasTable = await knex.schema.hasTable('events');
    if (!hasTable){
        return await knex.schema.createTable('events',(table)=>{
            table.increments('id');
            table.string('event_name').notNullable();
            table.date('event_date').notNullable();
            table.time('event_start_time').notNullable();
            table.time('event_end_time').notNullable();
            table.timestamps(false,true);
        });
    } else {
        return Promise.resolve();
    }
}

async function collect_points(knex:Knex){
    const hasTable = await knex.schema.hasTable('collect_points');
    if (!hasTable){
        return await knex.schema.createTable('collect_points',(table)=>{
            table.increments('id');
            table.string('name').notNullable();
            table.string('address').notNullable();
            table.timestamps(false,true);
        });
    } else {
        return Promise.resolve();
    }
}

async function event_collect_points(knex:Knex){
    const hasTable = await knex.schema.hasTable('event_collect_points');
    if(!hasTable){
        return await knex.schema.createTable('event_collect_points',(table)=>{
            table.increments('id');
            table.integer('collect_points_id').unsigned();
            table.foreign('collect_points_id').references('collect_points.id');
            table.integer('event_id').unsigned();
            table.foreign('event_id').references('events.id');
            table.timestamps(false,true);
        });
    } else {
        return Promise.resolve();
    }
}

async function organization_info(knex:Knex){
    const hasTable = await knex.schema.hasTable('organization_info');
    if(!hasTable){
        return await knex.schema.createTable('organization_info',(table)=>{
            table.increments('id');
            table.string('organization_name').notNullable();
            table.string('organization_address').notNullable();
            table.string('contact_num').notNullable();
            table.string('contact_person').notNullable();
            table.string('BR_num').notNullable();
            table.timestamps(false,true);
        });
    } else {
        return Promise.resolve();
    }
}
async function organization_event_registers(knex:Knex){
    const hasTable = await knex.schema.hasTable('organization_event_registers');
    if(!hasTable){
        return await knex.schema.createTable('organization_event_registers',(table)=>{
            table.increments('id');
            table.integer('volunteer_num').notNullable().unsigned();
            table.boolean('isSubmitted').defaultTo(false);
            table.integer('event_id').unsigned();
            table.foreign('event_id').references('events.id');
            table.integer('organization_info_id').unsigned();
            table.foreign('organization_info_id').references('organization_info.id');
            table.timestamps(false,true);
        });
    } else {
        return Promise.resolve();
    }
}

async function roles(knex:Knex){
    const hasTable = await knex.schema.hasTable('roles');
    if(!hasTable){
        return await knex.schema.createTable('roles',(table)=>{
            table.increments('id');
            table.string('name').notNullable();
            table.timestamps(false,true);
        });
    }else {
        return Promise.resolve();
    }
}

async function users(knex:Knex){
    const hasTable = await knex.schema.hasTable('users');
    if (!hasTable){
        return await knex.schema.createTable('users',(table)=>{
            table.increments('id');
            table.string('username').notNullable().unique();
            table.string('password').notNullable();
            table.string('telephone').notNullable();
            table.string('email').notNullable().unique();
            table.string('user_eng_fullname').notNullable();
            table.string('user_chi_fullname').notNullable();
            table.string('contact_person').notNullable();
            table.integer('organization_info_id').unsigned();
            table.foreign('organization_info_id').references('organization_info.id');
            table.boolean('isdeleted').notNullable().defaultTo('false')
            table.timestamps(false,true);
        });
    } else {
        return Promise.resolve();
    }
}
async function volunteers(knex:Knex){
    const hasTable = await knex.schema.hasTable('volunteers');
    if(!hasTable){
        return await knex.schema.createTable('volunteers',(table)=>{
            table.increments('id');
            table.string('eng_fullname').notNullable();
            table.string('chi_fullname').notNullable();
            table.string('contact_person').notNullable();
            table.string('contact_num').notNullable();
            table.integer('user_id').unsigned();
            table.foreign('user_id').references('users.id');
            table.boolean('isdeleted').notNullable().defaultTo('false')
            table.timestamps(false,true);
        });
    } else {
        return Promise.resolve()
    }
}

async function event_registers(knex:Knex){
    const hasTable = await knex.schema.hasTable('event_registers');
    if(!hasTable){
        return await knex.schema.createTable('event_registers',(table)=>{
            table.increments('id');
            table.integer('event_id').unsigned();
            table.foreign('event_id').references('events.id');
            table.integer('volunteer_id').unsigned();
            table.foreign('volunteer_id').references('volunteers.id');
            table.timestamps(false,true);
        });
    } else{
        return Promise.resolve();
    } 
}



async function collect_point_leaders(knex:Knex){
    const hasTable = await knex.schema.hasTable('collect_point_leaders');
    if(!hasTable){
        return await knex.schema.createTable('collect_point_leaders',(table)=>{
            table.increments('id');
            table.integer('collect_point_id').unsigned();
            table.foreign('collect_point_id').references('collect_points.id');
            table.integer('user_id').unsigned();
            table.foreign('user_id').references('users.id');
            table.timestamps(false,true);
        })
    }
}

async function user_roles(knex:Knex){
    const hasTable = await knex.schema.hasTable('user_roles');
    if(!hasTable){
        return await knex.schema.createTable('user_roles',(table)=>{
            table.increments('id');
            table.integer('user_id').unsigned();
            table.foreign('user_id').references('users.id');
            table.integer('role_id').unsigned();
            table.foreign('role_id').references('roles.id');
            table.timestamps(false,true);
        });
    }else {
        return Promise.resolve();
    }
}

async function flagbag_history(knex:Knex){
    const hasTable = await knex.schema.hasTable('flagbag_history');
    if(!hasTable){
        return await knex.schema.createTable('flagbag_history',(table)=>{
            table.increments('id');
            table.string('QR_code').notNullable();
            table.string('bag_status').notNullable();
            table.integer('collect_point_id').unsigned();
            table.foreign('collect_point_id').references('collect_points.id');
            table.integer('volunteer_id').unsigned();
            table.foreign('volunteer_id').references('volunteers.id');
            table.integer('organization_info_id').unsigned();
            table.foreign('organization_info_id').references('organization_info.id')
            table.integer('event_id').unsigned();
            table.foreign('event_id').references('events.id');
            table.timestamps(false,true);
        });
    } else {
        return Promise.resolve();
    }
}


export async function up(knex: Knex): Promise<any> {
    await events(knex);
    await collect_points(knex);
    await event_collect_points(knex);
    await organization_info(knex);
    await organization_event_registers(knex);
    await roles(knex);
    await users(knex);
    await volunteers(knex);
    await event_registers(knex);
    await collect_point_leaders(knex);
    await user_roles(knex);
    await flagbag_history(knex);
    
};


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTableIfExists('flagbag_history');
    await knex.schema.dropTableIfExists('user_roles');
    await knex.schema.dropTableIfExists('collect_point_leaders');
    await knex.schema.dropTableIfExists('event_registers');
    await knex.schema.dropTableIfExists('volunteers');
    await knex.schema.dropTableIfExists('users');
    await knex.schema.dropTableIfExists('roles');
    await knex.schema.dropTableIfExists('organization_event_registers');
    await knex.schema.dropTableIfExists('organization_info');
    await knex.schema.dropTableIfExists('event_collect_points');
    await knex.schema.dropTableIfExists('collect_points');
    await knex.schema.dropTableIfExists('events');
    
};

