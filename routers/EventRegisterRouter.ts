import { EventRegisterService } from "../services/EventRegisterService";
import * as express from 'express';
import {Request,Response} from 'express';

export class EventRegisterRouter{

    constructor(private eventRegisterService:EventRegisterService){
    }

    router(){
        const router = express.Router();
        router.get('/user/:id',this.getUserEvent)
        router.get('/',this.get);
        router.post('/',this.post);
        router.put('/:id',this.put);
        router.delete('/:id',this.delete);
        return router;
    }

    getUserEvent = async(req:Request,res:Response)=>{
        try{
            const id = parseInt(req.params.id);
            res.json(await this.eventRegisterService.retrieveUserEvent(id))
        }catch(e){
            console.log(e);
            res.status(500);
            res.json({success:false})
        }
    }

    get = async (_req:Request,res:Response)=>{
        try{
            res.json(await this.eventRegisterService.retrieve());
        }catch(e){
            res.status(500);
            res.json({success:false})
        }
    }

    post = async (req:Request,res:Response)=>{
        try{
            await this.eventRegisterService.create(req.body.event_id,req.session.userId);
            res.json({success:true});
        }catch(e){
            console.log(e);
            res.status(500);
            res.json({success:false});
        }
    }

    put = async (req:Request,res:Response)=>{
        try{
            const updated = req.body; 
            const id = parseInt(req.params.id);
            await this.eventRegisterService.update(id,updated);
            res.json({success:true});
        }catch(e){
            console.log(e);
            res.status(500);
            res.json({success:false});
        }
    }

    delete = async (req:Request,res:Response)=>{
        try{
            const id = parseInt(req.params.id);
            await this.eventRegisterService.delete(id);
            res.json({success:true});
        }catch(e){
            console.log(e);
            res.status(500);
            res.json({success:false});
        }
    }

}