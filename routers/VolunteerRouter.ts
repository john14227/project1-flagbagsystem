import { VolunteerService } from '../services/VolunteerService';
import * as express from 'express';
import { Request, Response } from 'express';
import { any } from 'bluebird';

export class VolunteerRouter{

    constructor(private volunteerService:VolunteerService){
    }

    router(){
        const router = express.Router();
        router.get('/:eventId',this.get);
        router.put('/volunteerList',this.put);
        router.post('/submission',this.post);
        router.put('/:/id',this.put);
        router.delete('/reset',this.delete);
        return router;
    }

    get = async (req:Request,res:Response)=>{
        try{
            const eventId = req.params.eventId;
            console.log(req.session.userId);
            // console.log(num);
            const details = await this.volunteerService.retrieve(req.session.userId,eventId)
            console.log(details);
            await res.json(details);
        }catch(e){
            await res.status(500);
            await res.json({success:false})
        }
    }

    // req.session.userId

    
    post = async(req,res) => {
        try{
            const userId = req.session.userId;
            const eventId = req.body.eventId;
            const volunteerList = ((req.body.volunteerList)? (req.body.volunteerList).concat([]):[]);
            const organizationId = req.body.organizationId;    
            console.log('post:',eventId, organizationId, volunteerList);
            await this.volunteerService.submit(userId,organizationId,eventId,volunteerList)   
            res.json({success:true});     
        } catch(err){
            console.log(err);
            res.status(500);
            res.json({success:false});
        }

    }

    put = async (req:Request,res:Response)=>{
        

        //const qrCodeBatch = [];
        // volunteerList.forEach(volunteer => {
        //     volunteer.user_id = req.session.userId;
        //     //qrCodeBatch.push({QR_code:volunteer.QR_code});
        //     //delete volunteer.QR_code;
        // });

        try{
            const userId = req.session.userId;
            const eventId = req.body.eventId;
            const volunteerList = ((req.body.volunteerList)? (req.body.volunteerList).concat([]):[]);
            const organizationId = req.body.organizationId;        
            console.log('update:',eventId, organizationId, volunteerList);
            //console.log(await this.volunteerService.create(volunteerList. qrCodeBatch, eventId, userId));
            await this.volunteerService.update(userId,organizationId,eventId,volunteerList);
            res.json({success:true});
        }catch(e){
            console.log(e);
            res.status(500);
            res.json({success:false});
        }
    }


    // put = async (req:Request,res:Response)=>{
    //     try{
    //         const updated = req.body;
    //         const id = parseInt(req.params.id);
    //         await this.volunteerService.update(id,updated);
    //         res.json({success:true});
    //     }catch(e){
    //         console.log(e);
    //         res.status(500);
    //         res.json({success:false});
    //     }
    // }

    delete = async (req:Request, res:Response) => {
        try{
            const userId = req.session.userId;
            const eventId = req.body.eventId
            const organizationId = req.body.organizationId;
            await this.volunteerService.delete(userId,eventId,organizationId);
        } catch(e){
            console.log(e);
            res.status(500);
            res.json({success:false});
        }
    }

    
}





