import { EventService } from "../services/EventService";
import * as express from 'express';
import {Request,Response} from 'express';


export class EventRouter{

    constructor(private eventService:EventService){
    }

    router(){
        const router = express.Router();
        router.get('/unregistered/',this.getUnregisteredEvent);
        router.get('/:id',this.getSingleEvent);
        router.post('/',this.post);
        router.put('/:id',this.put);
        router.delete('/:id',this.delete);
        return router;
    }

    getUnregisteredEvent = async (req:Request,res:Response) =>{
        try{
            const id = req.session.userId;
            res.json(await this.eventService.retrieveUnregisterObject(id));
        }catch(e){
            console.log(e);
            res.status(500);
            res.json({success:false});
        }
    }

    getSingleEvent = async (_req:Request,res:Response) =>{
        try{
            const id = parseInt(_req.params.id);
            res.json(await this.eventService.retrieveSingleObject(id)); 
        }catch(e){
            console.log(e);
            res.status(500);
            res.json({success:false});
        }
    }

    post = async (req:Request,res:Response)=>{
        const newEvent = req.body;
        try{
            await this.eventService.create(newEvent);
            res.json({success:true});
        }catch(e){
            console.log(e);
            res.status(500);
            res.json({success:false});
        }
    }

    put = async (req:Request,res:Response)=>{
        try{
            const updated = req.body; 
            const id = parseInt(req.params.id);
            await this.eventService.update(id,updated);
            res.json({success:true});
        }catch(e){
            console.log(e);
            res.status(500);
            res.json({success:false});
        }
    }

    delete = async (req:Request,res:Response)=>{
        try{
            const id = parseInt(req.params.id);
            await this.eventService.delete(id);
            res.json({success:true});
        }catch(e){
            console.log(e);
            res.status(500);
            res.json({success:false});
        }
    }

}