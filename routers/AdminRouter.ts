import { UserService } from "../services/UserService";
import { EventService } from '../services/EventService';
import { VolunteerService } from '../services/VolunteerService';
import { EventRegisterService } from '../services/EventRegisterService';
import * as express from 'express';
import {Request,Response} from 'express';
import {Validation} from '../ServerSideInputChecker';
import { FlagbagHistoryService } from "../services/FlagbagHistoryService";


export class AdminRouter{

    constructor(private userService:UserService,
                private inputValidation:Validation,
                private eventService:EventService,
                private volunteerService:VolunteerService,
                private eventRegisterService:EventRegisterService,
                private flagbagHistoryService:FlagbagHistoryService){
        
    }
        router(){
            const router = express.Router();
            router.get('/logout',this.logout);
            router.get('/get-all-volunteers',this.getAllVolunteers);
            router.get('/get-all-users',this.getAllUsers);
            router.get('/get-all-events',this.getAllEvents);
            router.get('/get-all-flagbags',this.getAllFlagbags);

            router.get('/get-volunteer/:id',this.getSingleVolunteer);
            router.get('/get-user/:id',this.getSingleUser);
            router.get('/get-event/:id',this.getSingleEvent);
            

            router.post('/create-new-event/',this.createNewEvent);
            router.post('/create-new-user/',this.createNewUser);
            router.post('/create-new-flagbag/',this.createNewFlagbag);
            // router.post('/create-new-volunteer',this.createNewVolunteer);


            router.put('/update-user/:id',this.updateUser);
            router.put('/update-event/:id',this.updateEvent);
            router.put('/update-organization-event-register/:id',this.updateOrgEventReg);
            
            router.put('/delete-user/:id',this.deleteUser);
            router.put('/delete-volunteer/:id',this.deleteVolunteer);
            return router;
        }
    
        getAllUsers = async (_req:Request, res:Response) =>{
                res.json(await this.userService.retrieveAllUsers());
            }

        getAllEvents = async (_req:Request, res:Response)=>{
                res.json(await this.eventService.retrieve());
            }

        getAllVolunteers = async(_req:Request, res:Response)=>{
                //res.json(await this.volunteerService.retrieve())
            }
        
        getAllFlagbags = async(_req:Request,res:Response)=>{
            res.json(await this.flagbagHistoryService.retrieve());
            }

        getSingleUser = async (req:Request,res:Response) =>{
            const username = req.session.username;
            const userList = await this.userService.retrieve(username);
            let user = userList.find((record)=>{
                    return record.username = username;
                });
                res.json(user);
            }

        getSingleEvent = async (req:Request,res:Response) =>{
                const id = req.session.id;
                const eventList = await this.eventService.retrieveSingleObject(parseInt(id));
                let event = eventList.find((record)=>{
                    return record.id = id;
                });
                res.json(event);
            }

        getSingleVolunteer = async (req:Request,res:Response)=>{
                const id = req.session.id;
                const volunteer = await this.volunteerService.retrieveById(parseInt(id));
                res.json(volunteer);
            }
    
        createNewUser = async (req:Request,res:Response)=>{
            const newUser = req.body;
                await this.userService.create(newUser);
                res.json({ success:true });
            }

        createNewEvent = async(req:Request,res:Response)=>{
            const newEvent = req.body;
            console.log(newEvent);
        try{
            await this.eventService.create(newEvent);
            res.json({ success:true });
            }catch(e){
                console.log(e);
                res.status(500);
                res.json({success:false});
            }
        }
        
        createNewFlagbag = async(req:Request,res:Response)=>{
            const newFlagbag = req.body;
            console.log(newFlagbag);
            try{
                await this.flagbagHistoryService.create(newFlagbag);
                res.json({success:true});
            }catch(e){
                console.log(e);
                res.status(500);
                res.json({success:false});
            }
        }
        // createNewVolunteer = async(req:Request,res:Response)=>{
        //     const newVolunteer = req.body;
        //     await this.volunteerService.create(newVolunteer);
        //     res.json({ success: true })
        //     }
    
        updateUser = async (req:Request,res:Response)=>{
            try{
                const updated = req.body;
                const id = parseInt(req.params.id);
                await this.userService.update(id,updated);
                res.json({ success:true });
            }catch(e){
                console.log(e);
                res.status(500);
                res.json({ success:false });
            }
        }

        updateEvent = async (req:Request,res:Response)=>{
            try{
                const updated = req.body;
                const id = parseInt(req.params.id);
                await this.eventService.update(id,updated);
                res.json({ success:true });
            }catch(e){
                console.log(e);
                res.status(500);
                res.json({ success:false })
            }
        }

        updateOrgEventReg = async (req:Request,res:Response)=>{
            try{
                const updated = req.body;
                const id = parseInt(req.params.id);
                await this.eventRegisterService.update(id,updated);
                res.json({ success:true });
            }catch(e){
                console.log(e);
                res.status(500);
                res.json({ success:false })
            }
        }


        deleteUser = async(req:Request,res:Response)=>{
            try{
                const id = parseInt(req.params.id);
                await this.userService.delete(id);
                res.json({ success:true });
            }catch(e){
                console.log(e);
                res.status(500);
                res.json({ success:false });
            }
        }

        deleteVolunteer = async(req:Request,res:Response)=>{
            try{
                const id = parseInt(req.params.id);
                //await this.volunteerService.delete(id);
                res.json({ success:true });
            }catch(e){
                console.log(e);
                res.status(500);
                res.json({ success:false })
            }
        }

        logout = async(req,res) => {
            await this.userService.logout(req,res);
        }
    
    
        registerInputValidation = [
            this.inputValidation.username,
            this.inputValidation.password,
            this.inputValidation.email,
            this.inputValidation.organizationBR,
            this.inputValidation.contactPerson,
            this.inputValidation.telephone,
        ]
        
    
         updateInputValidation = [
            this.inputValidation.password,
            this.inputValidation.email,
            this.inputValidation.organizationBR,
            this.inputValidation.contactPerson,
            this.inputValidation.telephone,
        ]
            
    }
