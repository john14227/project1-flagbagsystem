import { UserService } from "../services/UserService";
import * as express from 'express';
import {Request,Response} from 'express';
import {Validation} from '../ServerSideInputChecker';



// all /users route put here
export class UserRouter{

    //private UserService:UserService;
    //private UserApplication: UserApplication;

    constructor(private UserService:UserService, private inputValidation:Validation){
        // this.UserService = new UserService();
        // this.UserApplication = new UserApplication();
    }


    router(){
        const router = express.Router();
        //router.get('/:id',this.get);
        router.get('/currentUser',this.get);
        router.get('/logout',this.logout);
       // router.get('/:id',this.get);
        router.post('/',this.post);
        router.put('/currentUser/:id',this.updateInputValidation,this.put);
        
        // router.post('/',this.login);
        return router;
    }


    get = async (req:Request,res:Response) =>{
        try{
            const username = req.session.username;
            const userList = await this.UserService.retrieve(username);
            //const users= await this.UserService.retrieve();
            // const id = req.session.userId;
            // const info = users.find(function(user){
            //     return user.id == id;
            // });
            let user = userList.find((record)=>{
                return record.username = username;
            });
            res.json(user);
        }catch(err){
            console.log(err);
            res.status(500);
            res.json({success:false});
        }
    }

    post = async (req:Request,res:Response)=>{
        const newUser = req.body;
        console.log("in", newUser); 
        try{
           await this.UserService.create(newUser);
            res.json({success:true});
        }catch(e){
            console.log(e);
            res.status(500);
            res.json({success:false});
        }
    }

    put = async (req:Request,res:Response)=>{
        try{
            const updated = req.body;
            const id = parseInt(req.params.id);
            await this.UserService.update(id,updated);
            res.json({success:true});
        }catch(e){
            console.log(e);
            res.status(500);
            res.json({success:false});
        }
    }

    logout = async(req,res) => {
        await this.UserService.logout(req,res);
    }


    registerInputValidation = [
        this.inputValidation.username,
        this.inputValidation.password,
        this.inputValidation.organizationName,
        this.inputValidation.organizationAddress,
        this.inputValidation.organizationBR,
        this.inputValidation.email,
        this.inputValidation.contactPerson,
        this.inputValidation.contactPersoneng,
        this.inputValidation.telephone,
    ]
    

     updateInputValidation = [
        this.inputValidation.username,
        this.inputValidation.password,
        this.inputValidation.organizationName,
        this.inputValidation.organizationAddress,
        this.inputValidation.organizationBR,
        this.inputValidation.email,
        this.inputValidation.contactPerson,
        this.inputValidation.contactPersoneng,
        this.inputValidation.telephone,
    ]
        
}